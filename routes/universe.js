
/*
 * General or Universal API
 */

module.exports = function(app, userModel, LocationModel){


/*
 * Universal API to get location type
 */

	app.get('/universe/getLocationType/:traceid', function(req, res){

				    	
			LocationModel.findOne({traceid: req.params.traceid, enabled: true}, function (err, row) {

				if (row != null) {
					if (row.locationtype == 'mobile') {

						if (!req.session.loggedIn){
					        res.send({status:true, result: row.locationtype});
					        return;
					     }else{
					     	res.redirect('/mobile/getRequestStatus/' + req.params.traceid);
					     	return;	
					     }
						
					}else{
						res.send({status:true, result: row.locationtype});
					}
					
				}else{
					console.log("Cannot get request" + err)
					res.send({status: false, reason: 'trace code does not exist or destroyed'});
				}
			});
	    
	});

	
};
