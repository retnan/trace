
/*
 * Event Location API
 */

module.exports = function(app, userModel, LocationModel, eventModel){


/*
 * User Event handler - To collect data in one blow, replace session with body
 */
	app.post('/event/createEvent', function(req, res){
	
		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }


	     userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

			if (row.EventLocation_id == null) {

				if(req.body.category.length == 0 || req.body.longitude.length == 0 || req.body.latitude.length == 0 || req.body.nameofplace.length == 0){
					res.send({status:false, reason: "Please fill in the required fields"})
					return;
				}

				var md5 = require('MD5');

				var data = new eventModel({
											'location_id': null,
											'uid': req.session.uid,
											'nameofplace': req.body.nameofplace,
											'category': req.body.category,
											'note' : req.body.note,
											'address' : req.body.address,
											'enabled': true,
											'timestamp': new Date(),
											'lastupdated': new Date(),
											'expires': "-1",
											'ispublic' : req.body.ispublic,
											'city': req.body.city
										});

				var eventLocation = new LocationModel({
											'uid': req.session.uid,
											'gps': 	{
													'longitude': req.body.longitude,
													'latitude': req.body.latitude
													},
											'altitude': req.body.altitude,
											'lastupdated': new Date(),
											'expires': new Date(),
											'enabled': true,
											'locationtype': 'events',
											'nearby' : req.body.nearby, //TODO
											'verifyChange': md5(Date.now())
										});

				eventLocation.traceid = md5(eventLocation._id.toString())

				eventLocation.save(function(err, row){

					if(!err){

						data.location_id = row._id
						data.traceid = md5(row._id.toString())

						data.save()
						userModel.update({usercode: req.session.usercode, enabled: true}, {EventLocation_id: row._id}).exec()
						req.session.onEvent = false
						res.send({status:true})
					}else{
						console.log("Error registring event location for user")
						res.send({status:false});

					}
				})
			}else{
				res.send({status:false, reason: "Destroy existing event first"});
			}
		});

		
	});

/*
 * event API to grap all details and gps cordinate
 */

	app.get('/event/getLocationDetails/:traceid', function(req, res){

		if (req.params.traceid.length < 3) {
			res.send({status: false, result: null})
			return;
		};

		eventModel.find({traceid: req.params.traceid}).populate('location_id').exec(function (err, row){

			if (row.length > 0) {

				if (typeof row[0].location_id != "undefined") {

					if (row[0].location_id.enabled) {
						res.send({status: true, result: row[0]})	
					}else{
						res.send({status: false, result: null})
					}
				};
					
			}else{
				res.send({status: false, result: null})
			}
		})
	     
	});


/*
 * Event API: Create event only if the EventLocation_id of the usermodel is null
 */

	function canCreateEvent(usercode){

		userModel.findOne({usercode: usercode, enabled: true}, function (err, row) {

			if (row.EventLocation_id == null) {
				return true
			}else{
				return false;
			}
		});

		return false;
	}

/*
 * Event API to check if user can create Event
 */

	app.get('/event/canCreate/', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	     if(canCreateEvent(req.session.usercode)){
	     	res.send({status: true});
	     	return
	     }else{
	     	res.send({status: false, reason: "Destroy event first"});
	     }
	});


/*
 * Event API to get active event if available
 */

	app.get('/event/getActiveEvent/', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: 1});
	        	return;
	     }
	     

	     /*
		"Please login first." : 1
		"User has no event" : 2
	     */

	    userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

	    		var myEventTraceID;

				if (row.EventLocation_id) {

					myEventTraceID = row.EventLocation_id;
					var md5 = require('MD5')
					var traceID = md5(myEventTraceID)
					LocationModel.findOne({_id: myEventTraceID, enabled: true}, function (err, row){
						//console.log(row.toString())
						//res.send({status: true, result: row.toString()})
						res.send({status: true, result: row})
					})
					

				}else{
					console.log("No valid event" + err)
					res.send({status: false, reason: 2});
					return;
				}
		});

	     
	});

/*
 * Event API to check if event is active (Public request - No session is required) and return the GPS cordinate if active
 */

	app.get('/event/isActiveEvent/:traceid', function(req, res){

		LocationModel.findOne({traceid: req.params.traceid, enabled: true}, function (err, row){

			if (row) {
				res.send({status: true, result: row})
			}else{
				res.send({status: false, result: null})
			}
			
					
		})
	     
	});


	app.get('/event/getLocationDetails/:traceid', function(req, res){

		if (req.params.traceid.length < 3) {
			res.send({status: false, result: null})
			return;
		};

		eventModel.find({traceid: req.params.traceid}).populate('location_id').exec(function (err, row){

			if (row.length > 0) {

				if (row[0].location_id.enabled) {
					res.send({status: true, result: row[0]})	
				}else{
					res.send({status: false, result: null})
				}	
			}else{

				res.send({status: false, result: null})
			}
		})
	     
	});
/*
 * Event API to get active event details if available
 */

	app.get('/event/getActiveEventDetails/', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	    userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

	    		var myEventTraceID;

				if (row.EventLocation_id) {

					myEventTraceID = row.EventLocation_id;
					var md5 = require('MD5')
					var traceID = md5(myEventTraceID)

					LocationModel.findOne({_id: myEventTraceID, enabled: true}, function (err, row){

						eventModel.findOne({location_id: row._id, uid: req.session.uid}, function (err, row){
							res.send({status: true, result: row})
						})
						
					})
					

				}else{
					console.log("Fatal Error, How come user doesn't exists:" + err)
					res.send({status: false, reason: "No valid event"});
					return;
				}
		});

	     
	});
/*
 * Event API to destroy Event Trace
 */

	app.get('/event/destroyMe/', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	    
	    userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

	    		var myEventTraceID;

				if (row.EventLocation_id) {

					myEventTraceID = row.EventLocation_id;

					userModel.update({usercode: req.session.usercode, enabled: true}, {EventLocation_id: null}).exec();
					var md5 = require('MD5')
					var traceID = md5(myEventTraceID) //still not sure if this works. if it doen't work. it should break in mobile event
					LocationModel.update({uid: req.session.uid, _id: myEventTraceID}, {enabled: false}).exec();
					res.send({status: true})

				}else{
					console.log("Fatal Error, How come user doesn't exists:" + err)
					res.send({status: false, reason: "Request has expired"});
					return;
				}
		});

	    
	});

	
};
