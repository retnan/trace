
/*
 * GET user Routes and Actions.
 */

module.exports = function(app, userModel, LocationModel, eventModel, workModel, HomeModel){


/*
 * User Profile page
 */
	app.get('/user', function(req, res){

		res.redirect('/static/profile.html')
	});

 
	app.get('/user/isLogedIn', function(req, res){


		if(typeof req.session === 'undefined'){
			res.send({status: false});
			return;
		}else{

			if(typeof req.session.loggedIn === 'undefined'){
				res.send({status: false});
				return;
			}
		}

		if (!req.session.loggedIn){
	        res.send({status: false});
	    }else{
	    	res.send({status: true});
	    }

	});

/*
 * Mobile Location Handler
 */
 
	app.post('/user/mobile', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

		if(req.body.longitude.length == 0 || req.body.latitude.length == 0 ){
			res.send({status:false})
			return;
		}

		var mobileLocation = new LocationModel({
									'uid': req.session.uid,
									'gps': 	{
											'longitude': req.body.longitude,
											'latitude': req.body.latitude
											},
									'altitude': req.body.altitude,
									'lastupdated': new Date(),
									'timestamp': new Date(),
									'expires': new Date(),// + 2hrs,// TODO, specify date in the future
									'enabled': true,
									'locationtype': 'mobile'
								});

		var md5 = require('MD5');
		mobileLocation.traceid = md5(mobileLocation._id.toString())

		mobileLocation.save(function(err){

			if(!err){
				console.log("registered mobile location for user")
				userModel.update({usercode: req.session.usercode, enabled: true}, {MobileLocation_id: mobileLocation._id}, function(err){
				res.send({status:true})
				})

			}else{
				console.log("Error registring mobile location for user")
				res.send({status:false})
			}
		})
		
	});


	app.post('/user/workplacegeo', function(req, res){

		if (!req.session.onWork) {
			res.send({status: false})
		}

		if(req.body.longitude.length == 0 || req.body.latitude.length == 0){
			res.send({status:false, reason:"nogps"})
			return;
		}

		req.session.longitude = req.body.longitude
		req.session.latitude = req.body.latitude
		req.session.altitude = req.body.altitude

		console.log("got Gps cordinates")
		res.send({status:true})
	});

	app.post('/user/workplacesearch', function(req, res){

		if (!req.session.onWork) {
			req.session.onWork = true
		}

		if (req.body.workname.length == 0 || req.body.city.length == 0) {
			res.send({status: false})
			return
		};
		
		req.session.workname = req.body.workname
		req.session.city = req.body.city

		console.log("captured work details" + req.body.workname + ", " + req.body.city)
		res.send({status: true})

	});

	app.post('/user/eventgeo', function(req, res){

		if (!req.session.onEvent) {
			req.session.onEvent = true
		}

		if(req.body.longitude.length == 0 || req.body.latitude.length == 0){
			res.send({status:false, reason:"nogps"})
			return;
		}

		req.session.longitude = req.body.longitude
		req.session.latitude = req.body.latitude
		req.session.altitude = req.body.altitude

		console.log("got Gps cordinates for events")
		res.send({status:true})
	});

/*
 * User Work page Handler - TO receive data in one blow, replace all req.session with req.body
 */
	app.post('/user/workplace', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	    if(true){
	    	/* Brown New Location  */

	    	if(req.session.workname.length == 0 || req.session.longitude.length == 0 || req.session.latitude.length == 0){
			res.send({status:false})
			return;
			}

			var workTraceID = null;
			//console.log("Usercode: " + req.session.usercode)

			userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

				if (!err) {
					workTraceID = row.workplaceLocation_id;//workplaceLocation_id
					console.log("Got the workplaceID: " + workTraceID)

					var md5 = require('MD5');

					locationTraceHashID = md5(workTraceID)

					var workDoc = 	{
										'gps': 	{
												'longitude': req.session.longitude,
												'latitude': req.session.latitude
												},
										'altitude': req.session.altitude,
										'lastupdated': new Date(),
										'expires': new Date(),
										'enabled': true,
										'nearby' : req.body.nearby,
										'verifyChange': md5(Date.now())
									};

					var data = 		{
											'worktag': req.body.worktag,
											'workname' : req.session.workname,
											'address' : req.body.address,
											'nearby': req.body.nearby,
											'city': req.session.city,
											'lastupdated': new Date()
									};

					LocationModel.update({_id: workTraceID}, workDoc).exec();

					workModel.update({traceid: locationTraceHashID}, data).exec();
					res.send({status: true})

				}else{
					console.log("Fatal Error, How come user doesn't exists:" + err)
					res.send({status: false, reason: "how come you don't exist"});
				}
			});			

	    }else{
	    	//copy existing data, into $this user's document
	    	//TODO in Trace-2.0

	    }
		
	});

/*
 * User Event handler - To collect data in one blow, replace session with body
 */
	app.post('/user/event', function(req, res){
	
		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

		if(req.body.category.length == 0 || req.session.longitude.length == 0 || req.session.latitude.length == 0 || req.body.nameofplace.length == 0){
			res.send({status:false})
			return;
		}

		var md5 = require('MD5');

		var data = new eventModel({
									'location_id': null,
									'uid': req.session.uid,
									'nameofplace': req.body.nameofplace,
									'category': req.body.category,
									'note' : req.body.note,
									'address' : req.body.address,
									'enabled': true,
									'timestamp': new Date(),
									'lastupdated': new Date(),
									'expires': "-1",
									'ispublic' : req.body.ispublic,
									'city': req.body.city
								});

		var eventLocation = new LocationModel({
									'uid': req.session.uid,
									'gps': 	{
											'longitude': req.session.longitude,
											'latitude': req.session.latitude
											},
									'altitude': req.session.altitude,
									'lastupdated': new Date(),
									'expires': new Date(),
									'enabled': true,
									'locationtype': 'events',
									'nearby' : req.body.nearby, //TODO
									'verifyChange': md5(Date.now())
								});

		eventLocation.traceid = md5(eventLocation._id.toString())

		eventLocation.save(function(err, row){

			if(!err){

				data.location_id = row._id
				data.traceid = md5(row._id.toString())

				data.save()
				userModel.update({usercode: req.session.usercode, enabled: true}, {EventLocation_id: row._id}).exec()
				req.session.onEvent = false
				res.send({status:true})
			}else{
				console.log("Error registring event location for user")
				res.send({status:false});

			}
		})



	});


/*
 * Logout Handler
 */
	app.get('/user/logout', function(req, res){

		req.session.loggedIn = false
		req.session.uid = null
		req.session.usercode = null
		res.send({status:true});
	});
	
/*
 * Handler - User Login page
 */
	app.post('/user/login', function(req, res){

		/*if (!req.session.loggedIn) {
	                return res.send({status:true});
        }*/
	
	    console.log("loging in..");
	    //return;

		//console.log(req.body);

		if(req.body.usercode.trim().length == 0 || req.body.passcode.trim().length == 0){
			res.send({status:false})
			return;
		}

		var md5 = require('MD5');

		var data = new userModel({
									'usercode' : md5(req.body.usercode.trim().toLowerCase()), 
									'passcode' : md5(req.body.passcode.trim().toLowerCase()),
									'enabled': true
								});

		userModel.findOne({usercode: data.usercode, passcode: data.passcode, enabled: data.enabled}, function (err, row) {

			if (row != null) {
				console.log("Authentication Success")
				console.log(row)
				//start session
				req.session.loggedIn = true
				req.session.uid = row._id
				req.session.usercode = row.usercode
				
				res.send({status:true});
			}else{
				console.log("login Error! Reason:" + err)
				res.send({status: false});
			}

		});

	});



	app.post('/user/registerUserNew', function(req, res){


		if(req.body.usercode.trim().length == 0 || req.body.passcode.trim().length == 0 || req.body.animal.trim().length == 0 || req.body.mother.trim().length == 0 || req.body.space.trim().length == 0){
			res.send({status:false, reason: 'All fields are required'})
			return;
		}

		var md5 = require('MD5');

		
		userModel.findOne({usercode: md5(req.body.usercode.trim().toLowerCase())}, function (err, row) {

			//console.log("Row: "+ row + " WHILE ERROR: " + err)
			if (row) {

				res.send({status:false, reason: "The username entered exists."});
				return;
			};

			/* Supported Location types: home, work, mobile, event*/
			var home = new LocationModel({
										'locationtype' : 'home',
										'timestamp' : new Date(), 
										'lastupdated' : new Date(),
										'enabled': false, //GPS not set yet
										'verifyChange': md5(Date.now())
									});

			var work = new LocationModel({
										'locationtype' : 'work',
										'timestamp' : new Date(), 
										'lastupdated' : new Date(),
										'enabled': false, //GPS not set yet
										'verifyChange': md5(Date.now())
									});
		
			var data = new userModel({
										'usercode' : md5(req.body.usercode.trim().toLowerCase()),
										'passcode' : md5(req.body.passcode.trim().toLowerCase()),
										'securityAnimal' : req.body.animal.trim(),
										'securityMother' : req.body.mother.trim(),
										'securitySpace' : req.body.space.trim(), 
										'securityHoneymoon' : null, 
										'securityBook' : null,
										'timestamp' : new Date(), 
										'lastupdated' : new Date(),
										'enabled': true,
										'homeLocation_id': home._id,
										'workplaceLocation_id': work._id,
										'MobileLocation_id': null,
										'EventLocation_id': null
									});

			var homedata = new HomeModel({
										'timestamp' : new Date(), 
										'lastupdated' : new Date()
									});

			var workdata = new workModel({
										'timestamp' : new Date(), 
										'lastupdated' : new Date()
									});

			data.save(function (err) {

				if (!err) {
					// Location info for home and work
					home.uid = data._id
					home.traceid = md5(home._id.toString())

					work.uid = data._id
					work.traceid = md5(work._id.toString())

					//Initialzie home info for user
					homedata.uid = data._id
					homedata.location_id = home._id
					homedata.traceid = md5(home._id.toString())

					//Initialzie work info for user
					workdata.uid = data._id
					workdata.location_id = work._id
					workdata.traceid = md5(work._id.toString())

					workdata.save(function(err){})
					homedata.save(function(err){})
					home.save(function(err){})
					work.save(function(err){})

					res.send({status:true})
				}else{
					res.send({status:false});
				}

			});

			//console.log(data);

		});


	});
	
};
