
/*
 * Event Location API
 */

module.exports = function(app, userModel, LocationModel, HomeModel){


/*
 * Update details at ones
 */

	app.post('/home/updateDetailsOnce', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	     if (req.body.city.length == 0 || req.body.longitude.length == 0 || req.body.latitude.length == 0) {
	     	res.send({status: false, reason: "Please try again!"});
	    	return;
	     };

	    var city = req.body.city;
	    var address = req.body.address;
	    var nearby = req.body.nearby;
	    var longitude = req.body.longitude;
	    var latitude = req.body.latitude;
	    var altitude = req.body.altitude;

	    userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

	    		var myHomeTraceID;

				if (row.homeLocation_id) {
					myHomeTraceID = row.homeLocation_id;

					var md5 = require('MD5')
					var traceId = md5(myHomeTraceID)//i dont trust this

					HomeModel.update({uid: req.session.uid}, {address: address, location_id: myHomeTraceID,traceid: traceId, city: city, nearby: nearby, lastupdated: new Date()}).exec();
					LocationModel.update({uid: req.session.uid}, {traceid: traceId, gps:{longitude:longitude,latitude:latitude},altitude:altitude,lastupdated: new Date(),enabled:true}).exec();
					//trust in the LORD that all is well
					res.send({status: true});
				}else{
					console.log("Happy Error, Request has expired or destroyed:" + err)
					res.send({status: false, reason: "Request has expired or destroyed"});
					return;
				}
		});	
	});


/*
 * Event API to get the current logged in home tracecode
 */

	app.get('/home/getMyTraceCode/', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

		userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row){

			if (row) {
				var md5 = require("MD5")
				res.send({status: true, result: md5(row.homeLocation_id)})
			}else{
				res.send({status: false, result: null})
			}
		})
	     
	});

/*
 * Event API to check if event is active (Public request - No session is required) and return the GPS cordinate if active
 */

	app.get('/home/getLocation/:traceid', function(req, res){

		LocationModel.findOne({traceid: req.params.traceid, enabled: true}, function (err, row){

			if (row) {
				res.send({status: true, result: row})
			}else{
				res.send({status: false, result: null})
			}
		})
	     
	});

/*
 * Home API to grap all details and gps cordinate
 */

	app.get('/home/getLocationDetails/:traceid', function(req, res){

		if (req.params.traceid.length < 3) {
			res.send({status: false, result: null})
			return;
		};

		HomeModel.find({traceid: req.params.traceid}).populate('location_id').exec(function (err, row){

			if (row.length > 0) {
				if (row[0].location_id.enabled) {
					res.send({status: true, result: row[0]})	
				}else{
					res.send({status: false, result: null})
				}	
			}else{
				res.send({status: false, result: null})
			}
		})
	     
	});

	app.post('/home/updateLocation', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	    userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

	    		var myHomeTraceID;

	    		console.log(row);

				if (row.homeLocation_id) {
					myHomeTraceID = row.homeLocation_id;

					var md5 = require('MD5')
					var traceId = md5(myHomeTraceID)//i dont trust this

					LocationModel.update({uid: req.session.uid}, {traceid: traceId, gps:{longitude:req.body.longitude,latitude:req.body.latitude},altitude:req.body.altitude,lastupdated: new Date(),enabled:true}, function(row){
						console.log("mongodb response="+row)
					});

					res.send({status: true})
				}else{
					console.log("Happy Error, Request has expired or destroyed:" + err)
					res.send({status: false, reason: "Request has expired or destroyed"});
					return;
				}
		});	
	});
}