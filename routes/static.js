
/*
 * Static APIs
 */

module.exports = function(app){

	function renderForMobile(output, template){

		app.render(template, {title: 'Trace'}, function(err, html) {

		  var html = html.replace("\"/static","\"static");
		  var html = html.replace(/\"\/static\//g,"\"static/");
		  html = html.replace(/\"\/javascripts\//g,"\"javascripts/");
		  html = html.replace(/\"\/stylesheets\//g,"\"stylesheets/");
		  
		  require('fs').writeFile(process.cwd() + '/cordova/trace/www' + output, html);

		});
	}


	app.get('/static/index.html', function(req, res){

		res.render('index', { title: 'Trace!' });

		app.render('index', {title: 'Trace'}, function(err, html) {
		  //var html = html.replace(/\"\/static\//g,"\"static/");
		  var html = html.replace(/\"\/images\//g,"\"images/");
		  html = html.replace(/\"\/javascripts\//g,"\"javascripts/");
		  html = html.replace(/\"\/stylesheets\//g,"\"stylesheets/");
		  
		  require('fs').writeFile(process.cwd() + '/cordova/trace/www/index.html', html)
	
		});

	});
/*
* Mobile stuff
*/
	app.get('/static/mobile.html', function(req, res){

		res.render('mobile', {title: 'Trace'});
		renderForMobile("/static/mobile.html", 'mobile');
	});

	app.get('/static/mobilemultimap.html', function(req, res){

		res.render('mobilemultimap', {title: 'Trace'});
		renderForMobile("/static/mobilemultimap.html", 'mobilemultimap');
	});

	app.get('/static/mobilemakerequest.html', function(req, res){

		res.render('mobilemakerequest', {title: 'Trace'});
		renderForMobile("/static/mobilemakerequest.html", 'mobilemakerequest');
	});

	app.get('/static/workmap.html', function(req, res){

		res.render('workmap', {title: 'Trace'});
		renderForMobile("/static/workmap.html", 'workmap');
	});

	app.get('/static/eventmap.html', function(req, res){

		res.render('eventmap', {title: 'Trace'});
		renderForMobile("/static/eventmap.html", 'eventmap');
	});

	app.get('/static/mobilemap.html', function(req, res){

		res.render('mobilemap', {title: 'Trace'});
		renderForMobile("/static/mobilemap.html", 'mobilemap');
	});

	app.get('/static/mobileinit.html', function(req, res){

		res.render('mobileinit', {title: 'Trace'});
		renderForMobile("/static/mobileinit.html", 'mobileinit');

	});

	app.get('/static/eventinit.html', function(req, res){

		res.render('eventinit', {title: 'Trace'});
		renderForMobile("/static/eventinit.html", 'eventinit');
	});

/*
 * User Work page
 */
	app.get('/static/workplace.html', function(req, res){

		res.render('workplace', {title: 'Trace'});
		renderForMobile("/static/workplace.html", 'workplace');
	});

	app.get('/static/workresult.html', function(req, res){

		res.render('workplaceresult', {title: 'Trace'});
		renderForMobile("/static/workresult.html", 'workplaceresult');
	});

	app.get('/static/work.html', function(req, res){

		res.render('work', {title: 'Trace'});
		renderForMobile("/static/work.html", 'work');
	});

	app.get('/static/workplacegeo.html', function(req, res){

		res.render('workplacegeo', {title: 'Trace'});
		renderForMobile("/static/workplacegeo.html", 'workplacegeo');
	});

	app.get('/static/workplaceform.html', function(req, res){

		res.render('workplaceform', {title: 'Trace'});
		renderForMobile("/static/workplaceform.html", 'workplaceform');
	});

/*
 * User Event page
 */
	app.get('/static/event.html', function(req, res){

		res.render('event', {title: 'Trace'});
		renderForMobile("/static/event.html", 'event');
	});

	app.get('/static/settings.html', function(req, res){

		res.render('settings', {title: 'Trace'});
		renderForMobile("/static/settings.html", 'settings');
	});

	app.get('/static/eventform.html', function(req, res){

		res.render('eventform', {title: 'Trace'});
		renderForMobile("/static/eventform.html", 'eventform');
	});

	app.get('/static/help.html', function(req, res){

		res.render('help', {title: 'Trace'});
		renderForMobile("/static/help.html", 'help');
	});

	app.get('/static/tracepage.html', function(req, res){

		res.render('tracepage', {title: 'Trace'});
		renderForMobile("/static/tracepage.html", 'tracepage');
	});

	app.get('/static/tracemap.html', function(req, res){

		res.render('tracemap', {title: 'Trace'});
		renderForMobile("/static/tracemap.html", 'tracemap');
	});

	app.get('/static/help2.html', function(req, res){

		res.render('help2', {title: 'Trace'});
		renderForMobile("/static/help2.html", 'help2');
	});

	app.get('/static/profile.html', function(req, res){
	        //console.log(req.session.usercode)
	        renderForMobile("/static/profile.html", 'profile');
	        if (req.session.loggedIn){
	        	res.render('profile', {title: 'Trace'});
	        }else{
	        	res.send({status: false});
	        }
	});	
/*
 * User Login page
 */
 	
	app.get('/static/login.html', function(req, res){

		res.render('login', { title: 'Trace!' });
		renderForMobile("/static/login.html", 'login');
	});

	app.get('/static/home.html', function(req, res){

		res.render('home', { title: 'Trace!' });
		renderForMobile("/static/home.html", 'home');
	});

/*
 * User Registration Form
 */

	app.get('/static/register.html', function(req, res){

		res.render('register', { title: 'Trace!' });
		renderForMobile("/static/register.html", 'register');
	});

	app.get('/static/register1.html', function(req, res){

		res.render('register1', { title: 'Trace!' });
		renderForMobile("/static/register1.html", 'register1');
	});

	app.get('/static/register2.html', function(req, res){

		res.render('register2', { title: 'Trace!' });
		renderForMobile("/static/register2.html", 'register2');
	});

	app.get('/static/register3.html', function(req, res){

		res.render('register3', { title: 'Trace!' });
		renderForMobile("/static/register3.html", 'register3');
	});

	app.get('/static/register4.html', function(req, res){

		res.render('register4', { title: 'Trace!' });
		renderForMobile("/static/register4.html", 'register4');
	});

	app.get('/static/register5.html', function(req, res){

		res.render('register5', { title: 'Trace!' });
		renderForMobile("/static/register5.html", 'register5');
	});

	/*
	*	Home stuff
	*/
	app.get('/static/homeform.html', function(req, res){
		res.render('homeform', { title: 'Trace!' });
		renderForMobile("/static/homeform.html", 'homeform');
	})
};