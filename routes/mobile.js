
/*
 * Mobile Location API
 */

module.exports = function(app, userModel, LocationModel, mobileModel){

/*
 * Update location
 */
 
	app.post('/mobile/updateLocation', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	     userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {


	     	if (row.MobileLocation_id == null) {
	     		res.send({status: false, reason: "Mobile Trace code expired or destroyed"});
	        	return;
			}

			if(req.body.longitude.length == 0 || req.body.latitude.length == 0 ){
				res.send({status:false})
				return;
			}


			LocationModel.findOne({_id: row.MobileLocation_id, enabled: true}, function (err, axis) {

				if (axis != null) {
					axis.gps.longitude = req.body.longitude
					axis.gps.latitude = req.body.latitude
					axis.altitude = req.body.altitude
					axis.save();
					res.send({status:true});
					console.log("Updated Mobile Location");
				}else{				
					res.send({status: false, reason: 'Location not found or expired'});
				}
			});
		});
	});

/*
 * Mobile Location Handler
 */
 
	app.post('/mobile/createMobile', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	     userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

			if (row.MobileLocation_id != null) {
	     		res.send({status: false, reason: "Please Destroy the existing one first"});
	        	return;
			}

			if(req.body.longitude.length == 0 || req.body.latitude.length == 0 ){
				res.send({status:false})
				return;
			}

			var mobileLocation = new LocationModel({
										'uid': req.session.uid,
										'gps': 	{
												'longitude': req.body.longitude,
												'latitude': req.body.latitude
												},
										'altitude': req.body.altitude,
										'lastupdated': new Date(),
										'timestamp': new Date(),
										'expires': new Date(),// + 2hrs,// TODO, specify date in the future
										'enabled': true,
										'locationtype': 'mobile'
									});

			var md5 = require('MD5');
			mobileLocation.traceid = md5(mobileLocation._id.toString())

			mobileLocation.save(function(err){

				if(!err){
					console.log("registered mobile location for user")
					userModel.update({usercode: req.session.usercode, enabled: true}, {MobileLocation_id: mobileLocation._id}, function(err){
					res.send({status:true})
					})

				}else{
					console.log("Error registring mobile location for user")
					res.send({status:false})
				}
			})
		});
	});

	app.get('/mobile/canSeeTargetsOnMultiMap/', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

		userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

			
			if (row.MobileLocation_id != null) {
				var md5 = require('MD5')
				var traceid = md5(row.MobileLocation_id)

				mobileModel.find({traceid_b: traceid, authorized: '1', enabled: true}).populate('location_id_a').exec(function (err, row){

					if (row.length > 0) {
						res.send({status: true})				
					}else{
						res.send({status: false})
					}
				})

			}else{
				res.send({status: false, reason: 'I dont know'});
			}
		});

	     
	});

	/*
	if User with Tracecode "traceid_b" invited user with tracecode "traceid_b"
	the traceid_b must make this request to view the invited user's location (traceid_a's location)
	*/
	app.get('/mobile/getAllAuthorizedLocationDetails/', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

		userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

			
			if (row.MobileLocation_id != null) {
				var md5 = require('MD5')
				var traceid = md5(row.MobileLocation_id)

				mobileModel.find({traceid_b: traceid, authorized: '1', enabled: true}).populate('location_id_a').exec(function (err, row){

					if (row.length > 0) {
						res.send({status: true, result: row})				
					}else{
						res.send({status: false, result: null})
					}
				})

			}else{
				res.send({status: false, reason: 'I dont know'});
			}
		});

	     
	});

	/*
	if User with Tracecode "traceid_a" made a request to view user with tracecode "traceid_b"
	the traceid_a must make this request to view traceid_b's location
	*/

	app.get('/mobile/getBLocationDetails/:traceid', function(req, res){

		if (req.params.traceid.length < 3) {
			res.send({status: false, result: null})
			return;
		};


		userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

			
			if (row.MobileLocation_id != null) {
				var md5 = require('MD5')
				var traceid = md5(row.MobileLocation_id)

				mobileModel.find({traceid_a: traceid, traceid_b: req.params.traceid, authorized: '1', enabled: true}).populate('location_id_b').exec(function (err, row){

					if (row.length > 0) {

						if (typeof row[0].location_id_b != "undefined") {

							if (row[0].location_id_b.enabled) {
								res.send({status: true, result: row[0]})	
							}else{
								res.send({status: false, result: null})
							}
						};
							
					}else{
						res.send({status: false, result: null})
					}
				})

			}else{
				res.send({status: false, reason: 'I dont know'});
			}
		});

	     
	});


	app.get('/mobile/authorize/:docid', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

		userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

			
			if (row.MobileLocation_id != null) {
				var md5 = require('MD5')
				var traceid = md5(row.MobileLocation_id)

				mobileModel.update({_id: req.params.docid, traceid_b: traceid, enabled: true}, {authorized: '1'}).exec();

				res.send({status: true});
				
			}else{
				res.send({status: false, reason: 'I dont know'});
			}
		});
	});

/*
 * Event API to check if user can create Mobile; If not, then User's mobile Location is still active
 */

	app.get('/mobile/canCreate/', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

		userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

			if (row.MobileLocation_id == null) {
				res.send({status: false, reason: "Destroy mobile location first"});
	     		return
			}else{
				var md5 = require('MD5')
				res.send({status: true, traceid: md5(row.MobileLocation_id)});
	     		return		
			}
		});
	});



/*
 * Mobile API to destroy Trace
 */

	app.get('/mobile/destroyMe/', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	    
	    userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

	    		var myMobileTraceID;

				if (row.MobileLocation_id) {
					myMobileTraceID = row.MobileLocation_id;

					userModel.update({usercode: req.session.usercode, enabled: true}, {MobileLocation_id: null}).exec();
					var md5 = require('MD5')
					var traceId = md5(myMobileTraceID)//i dont trust this

					LocationModel.update({uid: req.session.uid, traceid: traceId}, {enabled: false}).exec();
					mobileModel.update({traceid_a: myMobileTraceID}, {authorized: '3', enabled: false}).exec();
					res.send({status: true})

				}else{
					console.log("Happy Error, Request has expired or destroyed:" + err)
					res.send({status: false, reason: "Request has expired or destroyed"});
					return;
				}
		});

	    
	});
/*
 * Mobile API to get Authorized Trace ids
 */

	app.get('/mobile/getAuthorizedTraceIds', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	    var myMobileTraceID;

	    userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

				if (row != null) {

					var md5 = require('MD5')
					myMobileTraceID = md5(row.MobileLocation_id);

					//get all traceID's of people you authorized to see my location

					var bigLocationResult = null

					mobileModel.find({traceid_b: myMobileTraceID, authorized: '1', enabled: true}, function (err, row) {

						if (row != null) {
							res.send({status: true, result: row});
						}else{
							console.log("No request" + err)
							res.send({status: false, reason: 'empty'});
						}
					});


				}else{
					console.log("Fatal Error, How come user doesn't exists:" + err)
					res.send({status: false, reason: "Request has expired"});
					return;
				}
		});


			    
	});

/*
 * Mobile API to check request status
 */

	app.get('/mobile/getRequestStatus/:traceid', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	    var myMobileTraceID;

	    userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

				if (row != null) {

					//checking if u can make request - U must have a valid mobile trace code to make a request
					if (row.MobileLocation_id == null) {
						res.send({status: true, result: 'mobile', reason: "cantmakerequest"});
						return;
					};

					var md5 = require('MD5')

					myMobileTraceID = md5(row.MobileLocation_id);

					//using session is a bad idea - Used here /mobile/makeRequest/
					//It should be entirely up for client implementations to send both here
					req.session.codetotrace = req.params.traceid;

					mobileModel.findOne({traceid_a: myMobileTraceID, traceid_b: req.params.traceid, enabled: true}, function (err, row) {

						if (row) {
							
							if(row.authorized == '0'){
								res.send({status: true, result: 'mobile', reason: "pending"});
								return;
							}else
							if(row.authorized == '1'){
								res.send({status: true, result: 'mobile', reason: "accepted"});
								return;
							}else
							if (row.authorized == '2') {
								res.send({status: true, result: 'mobile', reason: "rejected"});
								return;
							}else{
								res.send({status: true, result: 'mobile', reason: "I don't Know - Expired perhaps"});
								return;
							}
							
						}else{
							mobileModel.findOne({traceid_a: myMobileTraceID, traceid_b: req.params.traceid, enabled: false}, function (err, row) {

								if(row){
									res.send({status: true, result: 'mobile', reason: "expired"});
									return;
								}else{
									res.send({status: true, result: 'mobile', reason: "norequest"});
									return;
								}
							});
						}

					});					

				}else{
					console.log("Fatal Error, How come user doesn't exists:" + err)
					res.send({status: false, reason: "User's account disabled"});
					return;
				}
		});


	    
	});

/*
 * Mobile API to get target's current location
 */

	app.get('/mobile/getTargetCurrentLocation/:traceid', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	    var myMobileTraceID;

	    userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

				if (row != null) {

					var md5 = require('MD5')

					myMobileTraceID = md5(row.MobileLocation_id);


					//console.log("params: " + req.params.traceid + ", mine: " + myMobileTraceID)
					//get bonding
					mobileModel.findOne({'$or': [{traceid_a: myMobileTraceID, traceid_b: req.params.traceid}, {traceid_a: req.params.traceid, traceid_b: myMobileTraceID}], enabled: true, authorized: '1'}, function (err, row) {

						if (row) {

							LocationModel.findOne({traceid: req.params.traceid, enabled: true}, function (err, row) {

								if (row != null) {
									res.send({status:true, result: row});
								}else{
									console.log("Cannot get request" + err)
									res.send({status: false, reason: 'empty'});
								}
							});
						}else{
							res.send({status: false, reason: "Not authorized yet"});
						}
					});

				}else{
					console.log("Fatal Error, How come user doesn't exists:" + err)
					res.send({status: false, reason: "Request has expired"});
					return;
				}
		});


	    
	});
/*
 * Mobile API to make request to the supplied traceid, along side, long and lats
 */

	app.post('/mobile/makeRequest/', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	     targettraceid = req.session.codetotrace; /*TODO req.body.traceid*/

		if(req.body.nickname.length == 0 || targettraceid.length == 0){
			res.send({status:false})
			return;
		}

		userModel.findOne({usercode: req.session.usercode, enabled: true}, function(uerr, urow){

			if (!uerr) {

				if(urow){
					var md5 = require('MD5');
					mytraceid = md5(urow.MobileLocation_id);
					mylocationid = urow.MobileLocation_id;

					//you are not allowed to make request unless you have a valid mobile trace code

					if(mylocationid == null){
						res.send({status: false})
						return;
					}

					LocationModel.findOne({traceid: targettraceid, enabled: true}, function(lerr, lrow){

						if(!lerr){
							if (lrow) {
								var traceRequest = new mobileModel({
											'traceid_a': mytraceid,
											'traceid_b': targettraceid,
											'location_id_a': mylocationid,
											'location_id_b': lrow._id,
											'authorized': '0', //0 = pending, 1 = authorized, 2 = rejected
											'enabled': true, //false = expired
											'nickname': req.body.nickname,
											'timestamp': new Date(),
											'lastupdated': new Date()
									});

								traceRequest.save(function(err){});
								res.send({status:true})

							}else{
								res.send({status:false, reason: 'expired or destroyed'})
								return;
							}
						}else{
							res.send({status:false, reason: 'expired or destroyed'})
							return;
						}

					});

				}else{
					res.send({status:false})
				}		

			}else{
				res.send({status:false})
			}
		});		
	});
/*
 * Mobile API to check all request made to My TraceCode
 */

	app.get('/mobile/getRequest', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	    var mobileTraceID = null;

		userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

			if (row != null) {
				mobileTraceID = row.MobileLocation_id;

				if (mobileTraceID == null) {
					res.send({status: false, reason: 'empty'});
					return;
				};

				var md5 = require('MD5')
				locationTraceHashID = md5(mobileTraceID)

				/* Check the LocationModel for location expiring status before proceeding */

				mobileModel.find({traceid_b: locationTraceHashID, authorized: 0, enabled: true}, function (err, row) {

					if (row != null) {
						res.send({status:true, result: row});
					}else{
						console.log("No request" + err)
						res.send({status: false, reason: 'empty'});
					}
				});


			}else{
				console.log("Fatal Error, How come user doesn't exists:" + err)
				res.send({status: false, reason: "how come you don't exist"});
			}
		});
		
	});


/*
 * Mobile API to check all request made to My TraceCode
 */

	app.get('/mobile/getPendingRequest', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	    var mobileTraceID = null;

		userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

			if (row != null) {
				mobileTraceID = row.MobileLocation_id;

				if (mobileTraceID == null) {
					res.send({status: false, reason: 'empty'});
					return;
				};

				var md5 = require('MD5')
				locationTraceHashID = md5(mobileTraceID)

				/* Check the LocationModel for location expiring status before proceeding */

				mobileModel.findOne({traceid_a: locationTraceHashID, enabled: true}, function (err, row) {

					if (row != null) {
						res.send({status:true, result: row});
					}else{
						console.log("No request" + err)
						res.send({status: false, reason: 'empty'});
					}
				});


			}else{
				console.log("Fatal Error, How come user doesn't exists:" + err)
				res.send({status: false, reason: "how come you don't exist"});
			}
		});
		
	});
	
};
