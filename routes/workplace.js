
/*
 * Work Location API
 */

module.exports = function(app, userModel, LocationModel, workModel){


/*
 * User Work page Handler - TO receive data in one blow otherwise use the deprecated Function
 */
	app.post('/work/updateWorkplace', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

	    if(true){
	    	/* Brown New Location  */

	    	if(req.body.workname.length == 0 || req.body.longitude.length == 0 || req.body.latitude.length == 0 || req.body.city.length == 0  || req.body.worktag.length == 0){
				res.send({status:false})
				return;
			}

			var workTraceID = null;

			userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row) {

				if (!err) {
					workTraceID = row.workplaceLocation_id;//workplaceLocation_id

					var md5 = require('MD5');
					locationTraceHashID = md5(workTraceID)

					var workDoc = 	{
										'gps': 	{
												'longitude': req.body.longitude,
												'latitude': req.body.latitude
												},
										'altitude': req.body.altitude,
										'lastupdated': new Date(),
										'expires': new Date(),
										'enabled': true,
										'nearby' : req.body.nearby,
										'verifyChange': md5(Date.now())
									};

					var data = 		{
											'worktag': req.body.worktag,
											'workname' : req.body.workname,
											'address' : req.body.address,
											'nearby': req.body.nearby,
											'city': req.body.city,
											'lastupdated': new Date()
									};

					LocationModel.update({_id: workTraceID}, workDoc).exec();
					workModel.update({traceid: locationTraceHashID}, data).exec();
					//Trust in the LORD with all your heart
					res.send({status: true})

				}else{
					console.log("Fatal Error, How come user doesn't exists:" + err)
					res.send({status: false, reason: "how come you don't exist"});
				}
			});			

	    }else{
	    	//copy existing data, into $this user's document
	    	//TODO in Trace-2.0

	    }
		
	});

/*
 * Event API to get the current logged in workplace tracecode
 */

	app.get('/work/getMyTraceCode/', function(req, res){

		if (!req.session.loggedIn){
	        	res.send({status: false, reason: "Please login first."});
	        	return;
	     }

		userModel.findOne({usercode: req.session.usercode, enabled: true}, function (err, row){

			if (row) {
				var md5 = require("MD5")
				res.send({status: true, result: md5(row.workplaceLocation_id)})
			}else{
				res.send({status: false, result: null})
			}
		})
	});
	     
/*
 * get Workplace Location function. It returns the GPS cordinate if active or available
 */

	app.get('/work/getLocation/:traceid', function(req, res){

		LocationModel.findOne({traceid: req.params.traceid, enabled: true}, function (err, row){

			if (row) {
				res.send({status: true, result: row})
			}else{
				res.send({status: false, result: null})
			}
		})
	     
	});

	/*
 * get Workplace Details +  Location It returns the GPS cordinate if active or available alongside it's details
 */

	app.get('/work/getLocationDetails/:traceid', function(req, res){

		if (req.params.traceid.length < 3) {
			res.send({status: false, result: null})
			return;
		};

		workModel.find({traceid: req.params.traceid}).populate('location_id').exec(function (err, row){

			if (row.length > 0) {

				if (row[0].location_id.enabled) {
					res.send({status: true, result: row[0]})	
				}else{
					res.send({status: false, result: null})
				}	
			}else{

				res.send({status: false, result: null})
			}
		})
	     
	});

/*
 * Public request - No session is required). It returns detailed info about workplace available or not!
 */
	app.get('/work/getDetails/:locationid', function(req, res){

		workModel.findOne({location_id: req.params.locationid}, function (err, row){

			if (row) {
				res.send({status: true, result: row})
			}else{
				res.send({status: false, result: null})
			}
		})
	     
	});

	
};