
/*
 * GET home page.
 */

module.exports = function(app){

	app.get('/', function(req, res){
		res.redirect('/static/index.html')
	});

	app.get('/main', function(req, res){
		res.render('main')
	});
};
