//var tracehost = "http://localhost:3000";
var TESTCODE = "43ec10a9412aea30817511bf14e4ab8b";
//var tracehost = 'https://trace-dev-c9-retnan.c9.io';
var tracehost = 'http://traceroute.herokuapp.com';
var gmapscript = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAbEOIOoYMTyy7UspDQc5SQaLBySb3wI0w&sensor=true';

$( document ).ready(function() {

//Quick and Dirty Hack for Orientation changes. **TO BE REMOVED AFTER A GOOD IMPLEMENTATION IS FOUND***/

//console.log("Width: " + window.innerWidth);

if(window.innerWidth != window.innerHeight){
    setTimeout(kickOrientationListenerThread, 5000);
}

});


function kickOrientationListenerThread(){

        setInterval(function(){

        //var page = null;
        var page = $(':mobile-pagecontainer').pagecontainer('getActivePage')[0].id;

        //console.log(page);
        
        if (page == "homePage") {
            OrientationHack('Home');
        }else
        if (page == "work") {
            OrientationHack('Work');
        }else
        if (page == "workMap") {
            OrientationHack('WorkMap');
        }else
        if (page == "mobilepageinit") {
            OrientationHack('Mobile');
        }else
        if (page == "mobileMap") {
            OrientationHack('MobileMap');
        }else
        if (page == "mobilemultiMap") {
            OrientationHack('MobileMultiMap');
        }else
        if (page == "eventpageinit") {
            OrientationHack('Event');
        }else
        if (page == "eventMap") {
            OrientationHack('EventMap');
        }else
        if (page == "traceMap") {
            OrientationHack('HomeMap');
        };

    }, 1000);
}

function OrientationHack(page){

            //it's ok to show horizontal buttons if width of screen is greater
            if(window.innerWidth >= 480){

                if ($('#DisplayHorizontal' + page).is(':hidden')){
                    //console.log("landscape Set Time out");
                    $('#DisplayVertical' + page).hide().fadeOut('slow');
                    $('#DisplayHorizontal' + page).show().fadeIn('slow');
                }

                //console.log("No NEED");

                return;
            }  

            if(window.innerWidth < window.innerHeight){
                //console.log("Portrait Mode Set Time out ");


                if (!($('#DisplayHorizontal' + page).is(':hidden'))){
                    //console.log("Display Horinzontal is hidden");

                    $('#DisplayHorizontal' + page).hide().fadeOut('slow');
                    $('#DisplayVertical' + page).show().fadeIn('slow')
                }
                // else{
                //     console.log("No need to show Vertical")
                // }
                
            }else{

                if(Math.abs(window.innerWidth - window.innerHeight) > 100){

                    if ($('#DisplayHorizontal' + page).is(':hidden')){
                        //console.log("landscape Set Time out");
                        $('#DisplayVertical' + page).hide().fadeOut('slow');
                        $('#DisplayHorizontal' + page).show().fadeIn('slow');
                    }
                }else{

                    if (!($('#DisplayHorizontal' + page).is(':hidden'))){
                    //console.log("Display Horinzontal is hidden");

                        $('#DisplayHorizontal' + page).hide().fadeOut('slow');
                        $('#DisplayVertical' + page).show().fadeIn('slow')
                    }

                }
            }
    }

    // $(window).bind("orientationchange", function( event ) {

     
    //    // //not working - Not Firing..
    //    // if (event.orientation == 'portrait') {
    //    //      $('.OrientationDependent').attr('data-type', '');
    //    //  }else{
    //    //      $('.OrientationDependent').attr('data-type', 'horizontal');
    //    //  }

    //     if(window.innerWidth < window.innerHeight){
    //         //console.log("Portrait Mode ");
    //         $('.OrientationDependent').attr('data-type', '');
    //     }else{
    //         //console.log("landscape");
    //         $('.OrientationDependent').attr('data-type', 'horizontal');
    //     }

    // });

/* The problem here is that it runs onces */
    // $( document ).ready(function() {

    //     if(window.innerWidth < window.innerHeight){
    //         console.log("Portrait Mode ");
    //         $('.OrientationDependent').attr('data-type', '');
    //     }else{
    //         console.log("landscape");
    //         $('.OrientationDependent').attr('data-type', 'horizontal');
    //     }
    // });


function ajaxConnectionError(err){
        console.log("UNABLE TO CONNECT TO SERVER - COULD BE CROS ISSUES: " + err);
        $.mobile.loading( 'hide', {});//just incase
        console.log(err);
        showPopUp("Trace was unable to connect to the server.");
}



function showPopUp(message){

        var html = "<b>" + message + "</b>";

        $('.infoboxmesg').html(html);

        $('.retnaninfoBox').show();

        setTimeout(function(){

            $('.retnaninfoBox').hide();

        }, 3000);
}

function AuthorizeMobileRequest(doc_id){
    
        $.mobile.loading('show', {});
    
        $.ajax({
                    type: 'GET',
                    url: tracehost+'/mobile/authorize/' + doc_id,
                    data: null,
                    success: function(data) {

                            if(data.status){
                                $('#refreshCheckRequest').show();
                                $('#authorizeIt').hide();
                                $('#mRequestInfo').html("You currently do not have request. Hit refresh to see if there's more");
                                showPopUp("Authorized! click 'see target' to see target on your map");
                            }else{
                                showPopUp('Sorry, we were unable to authorize user, please try again: Server said: ' + data.reason);
                            }

                            $.mobile.loading('hide');
                    },
                    error: ajaxConnectionError,
                    contentType: "application/json",
                    dataType: 'json'
            });
}



(function (global) {
    
    /* GLobal Variables */

    var QRsize = 120;//'150x150';
    var QRencoding = 'UTF-8';
    var QRcorrection = 'L'; // | M | Q | H
    var QRcolor = "#166a83";
    //var QRcolor = "#e15613";

    //var directionsService = new google.maps.DirectionsService()
    var directionsService = null;

    var directionsDisplay = null;
    var gloTargetLatitude = null;
    var gloTargetLongitude = null;
    var marker = null;
    var map = null;
    var Gsingle_track_marker = null; //for tracking a single mobile target
    var Gsingle_track_marker_Mine = null; //The user doing the tracking

    var mthread_id1 = null; //thread handling 1-1 mobile targets
    var mthread_id2 = null; //thread handling 1-1 mobile targets
    var hthread_id = null; //home thread id
    var wthread_id = null; //work thread id
    var mmthread_id_slaves = null;
    var mmthread_id_master = null;

    var markers = Array();


    var GPSoptions = {
        frequency: 1000,
        enableHighAccuracy: true
    };


    function deviceSupportsLocalStorage() {
        return ('localStorage' in window) && window['localStorage'] !== null;
    }


    function showQRCode(page, content){

        //console.log("Fired SHOW QRCODE");
        if (page == "home") {

            $("#homeqrcode").kendoQRCode({
                value: content,
                errorCorrection: QRcorrection,
                size: QRsize,
                color: QRcolor,
                encoding: QRencoding,
                border: {
                            color: QRcolor, //"#67a814",
                            width: 2
                        }

            });

            return;
        }


        if (page == "work") {
            
            $("#workqrcode").kendoQRCode({
                value: content,
                errorCorrection: QRcorrection,
                size: QRsize,
                color: QRcolor,
                border: {
                            color: QRcolor, //"#67a814",
                            width: 2
                        }
            });

            return;
        }


        if (page == "event") {
            
            $("#eventqrcode").kendoQRCode({
                value: content,
                errorCorrection: QRcorrection,
                size: QRsize,
                color: QRcolor,
                border: {
                            color: QRcolor, //"#67a814",
                            width: 2
                        }
            });

            return;
        }


        if (page == "mobile") {
            
            $("#mobileqrcode").kendoQRCode({
                value: content,
                errorCorrection: QRcorrection,
                size: QRsize,
                color: QRcolor,
                border: {
                            color: QRcolor, //"#67a814",
                            width: 2
                        }
            });

            return;
        }
    }


    function formatTraceCode(tracecode){

        var result = ' ';
        result.trim();

        for (var i = 0; i < tracecode.length; i++) {

            result += tracecode[i];

            if (i % 8 == 0 && i != 0) {
                result += " - ";
            };
            
        };

        return result;
    }
    
    
    function setUpMap(locationData, panel, mcontainer){
        //setup the global variable to be used later
        gloTargetLatitude = locationData.gps.latitude;
        gloTargetLongitude = locationData.gps.longitude;
    
        directionsDisplay = new google.maps.DirectionsRenderer();
        //var destLat = 9.933333300000000000;
        //var destLong = 8.883333300000004000;
    
        var mapcontainer = mcontainer;
    
        var location = new google.maps.LatLng(gloTargetLatitude, gloTargetLongitude);
    
        var mapOptions = {
                zoom: 6,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: location
        };
    
        map = new google.maps.Map(document.getElementById(mapcontainer), mapOptions);
    
        marker = new google.maps.Marker({
              position: location,
              map: map,
              title: 'Hello World!'
        });
    
        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById(panel));
        console.log("Rendering google map");
    }
    
    
    function setUpMultiMap(locationDataArray, panel, mcontainer){
        //setup the global variable to be used later
        if(locationDataArray.length == 0){
            return false;
        }
    
        directionsDisplay = new google.maps.DirectionsRenderer();
    
        var mapcontainer = mcontainer;
    
        var location = new google.maps.LatLng("9.081999000000000000", "8.675277000000051000");
    
        var mapOptions = {
                zoom: 6,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: location
        };
    
        map = new google.maps.Map(document.getElementById(mapcontainer), mapOptions);
    
        for (var i = locationDataArray.length - 1; i >= 0; i--) {
    
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(locationDataArray[i].location_id_a.gps.latitude, locationDataArray[i].location_id_a.gps.longitude),
              map: map,
              title: locationDataArray[i].nickname
            });
        };
    
        /*marker = new google.maps.Marker({
              position: location,
              map: map,
              title: 'Hello World!'
        });*/
    
        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById(panel));
    }
    
    function computeRoute(myLat, myLong, mode) {


        if (typeof google == 'undefined') {
            //showPopUp("Sorry, to view map, restart traceApp with your device connected to internet");
            showPopUp("Please hit refresh.");
            return;
        }

            google.maps.event.trigger(map,'resize');
            map.setCenter(marker.getPosition());
    
            var origin = new google.maps.LatLng(myLat, myLong);
            var target = new google.maps.LatLng(gloTargetLatitude, gloTargetLongitude);
    
            var request = {
                origin: origin,
                destination: target,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
    
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    directionsDisplay.setMap(map);
                }
            });
    }

    function initializeMap(locationObject, infoPanelDiv, container){

        if (directionsService == null) {
            if (typeof google == 'undefined') {

                $.getScript(gmapscript, function() {
                    directionsService = new google.maps.DirectionsService();
                    setUpMap(locationObject, infoPanelDiv, container);
                });
                
                /*$.getScript(gmapscript)
                .done(function(script, textStatus) {
                    console.log(textStatus + " google map loaded, waiting for script execution at most 2 seconds before instatantiating google");
                    setTimeout(function(){
                        directionsService = new google.maps.DirectionsService();
                        setUpMap(locationObject, infoPanelDiv, container)
                    },2000);
                })
                .fail(function(jqxhr, settings, exception) {
                    console.log("retNAN error: possibly internet error");
                    showPopUp("Sorry, to view map, restart traceApp with your device connected to internet");
                });

                */
                //quick debugging script
                /*$.getScript(gmapscript, function(data, textStatus, jqxhr) {
                    console.log(data); //data returned
                    console.log(textStatus); //success
                    console.log(jqxhr.status); //200
                    console.log('Load was performed.');
                });*/

                //quick debugging script
                
                /*$.ajax({
                url: gmapscript,
                dataType: "script",
                success: function(suc){
                    console.log("succeful: " + suc);
                },
                error: function(err){
                    console.log("unable to fech script: " + err);
                }
                });*/

            }else{
                directionsService = new google.maps.DirectionsService();
                setUpMap(locationObject, infoPanelDiv, container)
            }
        }else{
            setUpMap(locationObject, infoPanelDiv, container);
        }

    }

    function initializeMultiMap(locationObject, infoPanelDiv, container){

        if (directionsService == null) {
            if (typeof google == 'undefined') {

                $.getScript(gmapscript)
                .done(function(script, textStatus) {
                    console.log(textStatus + " google map loaded, waiting for script execution at most 2 seconds before instatantiating google");
                    setTimeout(function(){
                        directionsService = new google.maps.DirectionsService();
                        setUpMultiMap(locationObject, infoPanelDiv, container)
                    },2000);
                })
                .fail(function(jqxhr, settings, exception) {
                    console.log("retNAN error: possibly internet error");
                    showPopUp("Sorry, to view map, restart traceApp with your device connected to internet");
                });

            }else{
                directionsService = new google.maps.DirectionsService();
                setUpMultiMap(locationObject, infoPanelDiv, container)
            }

        }else{
            setUpMultiMap(locationObject, infoPanelDiv, container)
        }
    }

    function setMapAtCenter(){
        if (typeof google == 'undefined') {
            //silence
        }else{
            google.maps.event.trigger(map,'resize');
            map.setCenter(marker.getPosition());
        }
    }
    
    function onGPSTrackError(Error){
    
        console.log(Error.message)
        $.mobile.loading( 'hide', {});

        if (Error.code == PositionError.PERMISSION_DENIED) {
            showPopUp('Sorry, permission to read your coordinate was denied');
        }else if(Error.code == PositionError.POSITION_UNAVAILABLE){
            showPopUp('Sorry, No coordinate available. Please, turn on your GPS then try again. User-Agent said: '+Error.message);
        }else if(Error.code == PositionError.TIMEOUT){
            showPopUp('No coordinate available. Please try again. User-Agent said: '+Error.message);
        }else{
            showPopUp('User-Agent said: '+Error.message);
        }
        
    }
    
    function onGPSTrackSuccess(position) {
    
                //var longitude = '9.845172000000048000';
                //var latitude = '10.309820000000000000';
                var longitude = position.coords.longitude;
                var latitude = position.coords.latitude;
                var altitude = -1;
    
                if (latitude.length == 0 || longitude.length == 0) {
                    $.mobile.loading( 'hide', {});
                    showPopUp('No coordinate available, Please try again');
                    return false;
                };
                //tell google map about my current location
                computeRoute(latitude, latitude, null);
                $.mobile.loading( 'hide', {});
    }

    function onGPSReadMobileSuccess(position){
        //var nickname = -1;
            var altitude = position.coords.altitude;
            var longitude = position.coords.longitude;
            var latitude = position.coords.latitude;
    
            if (latitude.length == 0 || longitude.length == 0) {
                $.mobile.loading( 'hide', {});
                showPopUp('No coordinate available, Please try again');
                return false;
            }

            $.ajax({
                type: 'POST',
                url: tracehost+'/user/mobile',
                data: JSON.stringify({/*nickname: nickname, */altitude: altitude, longitude: longitude, latitude: latitude}),
                success: function(data) {
                            if(data.status){
                                //get the returned tracecode and store in localHTML5 storage
                                $.mobile.loading( 'hide', {});
                                showPopUp('Success!');
                                setTimeout(function(){
                                    $.mobile.changePage("#mobilepageinit",{});
                                }, 3000);
                            }else{
                                $.mobile.loading( 'hide', {});
                                showPopUp('Oopse! Please verify your entry and try again. Traced returned: '+ data.reason);
                                return false;
                            }
                        },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
            });
    }


     $(document).on("pageinit", "#statPage", function(){

    
        $("#CheckForForSession").click(function(){

                $.mobile.loading('show');

                $.ajax({
                    type: 'GET',
                    url: tracehost+'/user/isLogedIn',
                    data: null,
                    success: function(data) {
                            
                            $.mobile.loading('hide');

                            if(data.status){
                                $.mobile.changePage("#profilePage",{});
                            }else{
                                $.mobile.changePage("#loginPage",{});
                            }
                    },
                    error: function(err){

                        $.mobile.loading('hide');

                        if (deviceSupportsLocalStorage()) {

                            if (localStorage.getItem("isLogedIn") == 'true') {
                                $.mobile.changePage("#profilePage",{});
                            }else{
                                $.mobile.changePage("#loginPage",{});
                            }

                        }else{
                            $.mobile.changePage("#loginPage",{});
                        }
                    },
                    contentType: "application/json",
                    dataType: 'json'
                });
        });
    
    });



    $(document).on("pagebeforecreate", "#traceMap", function(){
        updateTraceMapDOM();
    });


    function updateTraceMapDOM(){

        var traceid = $("input#codetotrace").val().trim();
        
        if(traceid.length == 0){
        
            if (deviceSupportsLocalStorage()) {
                traceid = localStorage.getItem("global.traceid"); //make false after use
            }
        }

        $.mobile.loading( 'show');
    
        $.ajax({
                type: 'GET',
                url: tracehost+'/home/getLocationDetails/' + traceid,
                data: null,
                success: function(data) {
                    
                        if(data.status){
                            initializeMap(data.result.location_id,'hinfoDirPanel','homemap_canvas');
                            setTimeout(function(){
                                $.mobile.loading( 'hide', {});
                                setMapAtCenter();
                                var output = "<address><p><em>Trace Code: </em><b> "+formatTraceCode(data.result.traceid)+"</b></p>" + "<p><em>Address:</em> "+data.result.address+"</p>"+ "<p><em>Nearby:</em> "+data.result.nearby+"</p>"+ "<p><em>City: </em> "+data.result.city+"</p></address>";
                                $('#hhExtraInfo').html(output);
                            },2000);
                        }else{
                            //the delay is neccessary to properly display the popup
                            setTimeout(function(){
                                $.mobile.loading( 'hide', {});
                                $('#homemap_canvas').remove();
                                showPopUp('The Trace Code does not exist or as expired.');
                            },2000);
                        }
                },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
        });

    }


    $(document).on("pageinit", "#traceMap", function(){
    
        $("#htrack").click(function(){
            navigator.geolocation.getCurrentPosition(onGPSTrackSuccess, onGPSTrackError, GPSoptions);
            $.mobile.loading('show', {});
        });
    
        $("#hrefresh").click(function(){
            updateTraceMapDOM();
        });
    
        $('#tracehome').click(function(){
            startTracingHomeTarget();
        });

        $("#htrack_h").click(function(){
            navigator.geolocation.getCurrentPosition(onGPSTrackSuccess, onGPSTrackError, GPSoptions);
            $.mobile.loading('show', {});
        });
    
        $("#hrefresh_h").click(function(){
            updateTraceMapDOM();
        });
    
        $('#tracehome_h').click(function(){
            startTracingHomeTarget();
        });
    
    });

    function startTracingHomeTarget(){
        //TODO: disable the trace icon here then remove the return
        //traceid is store in global var:= mobileGlobalTracid TODO: use later to compute distance
        //1: setup the custom marker
        //2: create the thread here with marker object as parameter
        //3: whenever the page title doesnt match #traceMap (user navigated away) then destroy thread
        //disable Trace 'n' Trace buttons, including refresh
        $("a#htrack").addClass('ui-disabled'); //vertical display
        $("a#htrack_h").addClass('ui-disabled'); //horizontal display
        $("a#tracehome").addClass('ui-disabled');
        $("a#tracehome_h").addClass('ui-disabled');
        $("a#hrefresh").addClass('ui-disabled');
        $("a#hrefresh_h").addClass('ui-disabled');

        _startTracingHomeTarget();
        hthread_id = setInterval(_startTracingHomeTarget, 1000 * 10); //30 seconds interval
    }


    function _startTracingHomeTarget(){

        //update location on map using custom icon
        //send location to server POST: /mobile/updateLocation 
        //params: {altitude, latitude, longitude}

         var page = $(':mobile-pagecontainer').pagecontainer('getActivePage')[0].id;
        //var page = "mobileMap";

        if (page != "traceMap") {
            //kill this thread, and enable trace button, since user has navigated away            
            clearInterval(hthread_id);
            //enable all buttons here
            $("a#htrack").removeClass('ui-disabled'); //vertical display
            $("a#htrack_h").removeClass('ui-disabled'); //horizontal display
            $("a#tracehome").removeClass('ui-disabled');
            $("a#tracehome_h").removeClass('ui-disabled');
            $("a#hrefresh").removeClass('ui-disabled');
            $("a#hrefresh_h").removeClass('ui-disabled');

            console.log("Timer stoped - reason: user navigated away");
        }

        console.log("_startTracingHomeTarget() thread started");

        console.log("read my location now");

        navigator.geolocation.getCurrentPosition(function(position){

            console.log("my location found");

            var longitude = position.coords.longitude;
            var latitude = position.coords.latitude;
            var altitude = position.coords.altitude;

            console.log("sending my location to server");

                                        //update location on map
                                        //Gsingle_track_marker_Mine

                if (typeof google == 'undefined') {
                    //quite
                }else{

                    location = new google.maps.LatLng(latitude, longitude);

                    if (Gsingle_track_marker_Mine == null) {

                        Gsingle_track_marker_Mine = new google.maps.Marker({
                            icon: {
                                path: google.maps.SymbolPath.CIRCLE,
                                scale: 10
                                },
                            position: location,
                            map: map,
                            title: 'Me!'
                        });

                    }else{

                        Gsingle_track_marker_Mine.setMap(null);
                        Gsingle_track_marker_Mine = null;

                        Gsingle_track_marker_Mine = new google.maps.Marker({
                            icon: {
                                path: google.maps.SymbolPath.CIRCLE,
                                scale: 10
                            },
                            position: location,
                            map: map,
                            title: 'Me!'
                        });
                    }

                    //console.log("Updating my market position");
                    //allready added in the marker property
                    //Gsingle_track_marker.setMap(map);
                }

                
                setTimeout(function(){

                    if (typeof google == 'undefined') {
                        //silence
                    }else{
                        console.log("resizing the map - because of change in my location");
                        google.maps.event.trigger(map,'resize');
                        map.setCenter(Gsingle_track_marker_Mine.getPosition());
                    }
                                                
                }, 2000);

        }, onGPSTrackError, GPSoptions);
    };

  
    $(document).on("pagebeforecreate", "#workMap", function(){
        updateWorkMapDOM();
    });

    function updateWorkMapDOM(){

        var traceid = $("input#codetotrace").val().trim();

        $.mobile.loading('show');
    
        $.ajax({
                type: 'GET',
                url: tracehost+'/work/getLocationDetails/' + traceid,
                data: null,
                success: function(data) {
                    
                        if(data.status){
                            
                            initializeMap(data.result.location_id,'winfoDirPanel', 'workmap_canvas')

                            setTimeout(function(){
                                $.mobile.loading( 'hide', {});
                                setMapAtCenter();
                                $('#wwExtraInfo').html("<address><p><em>Title:</em><b> "+data.result.workname+"</b></p>" + "<p><em>Address:</em> "+data.result.address+"</p>"+ "<p><em>Tag:</em> "+data.result.worktag+"</p>"+ "<p><em>City:</em> "+data.result.city+"</p></address>");
                            },2000);
                        }else{
                            //the delay is neccessary to properly display the popup
                            setTimeout(function(){
                                $.mobile.loading( 'hide', {});
                                $('#workmap_canvas').remove();
                                showPopUp('The Trace Code does not exist or as expired.');
                            },2000);
                        }
                },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
        });

    }
    
    $(document).on("pageinit", "#workMap", function(){

        $("#wtrack").click(function(){
            navigator.geolocation.getCurrentPosition(onGPSTrackSuccess, onGPSTrackError, GPSoptions);
            $.mobile.loading('show', {});
        });
    
        $("#wrefresh").click(function(){
            updateWorkMapDOM();
        });
    
        $('#tracework').click(function(){
            startTracingWorkTarget();
        });


        $("#wtrack_h").click(function(){
            navigator.geolocation.getCurrentPosition(onGPSTrackSuccess, onGPSTrackError, GPSoptions);
            $.mobile.loading('show', {});
        });
    
        $("#wrefresh_h").click(function(){
            updateWorkMapDOM();
        });
    
        $('#tracework_h').click(function(){
            startTracingWorkTarget();
        });
    });



    function startTracingWorkTarget(){
        //TODO: disable the trace icon here then remove the return
        //traceid is store in global var:= mobileGlobalTracid TODO: tobe used in computing distance
        //1: setup the custom marker
        //2: create the thread here with marker object as parameter
        //3: whenever the page title doesnt match #traceMap (user navigated away) then destroy thread
        //disable Trace 'n' Trace buttons, including refresh
        $("a#wtrack").addClass('ui-disabled'); //vertical display
        $("a#wtrack_h").addClass('ui-disabled'); //horizontal display
        $("a#tracework").addClass('ui-disabled');
        $("a#tracework_h").addClass('ui-disabled');
        $("a#wrefresh").addClass('ui-disabled');
        $("a#wrefresh_h").addClass('ui-disabled');
        _startTracingWorkTarget();
        wthread_id = setInterval(_startTracingWorkTarget, 1000 * 10); //30 seconds interval
    }


    function _startTracingWorkTarget(){

        //update location on map using custom icon
        //send location to server POST: /mobile/updateLocation 
        //params: {altitude, latitude, longitude}

         var page = $(':mobile-pagecontainer').pagecontainer('getActivePage')[0].id;
        //var page = "mobileMap";

        if (page != "workMap") {
            //kill this thread, and enable trace button, since user has navigated away            
            clearInterval(wthread_id);
            //TODO: Enable them here
            $("a#wtrack").removeClass('ui-disabled'); //vertical display
            $("a#wtrack_h").removeClass('ui-disabled'); //horizontal display
            $("a#tracework").removeClass('ui-disabled');
            $("a#tracework_h").removeClass('ui-disabled');
            $("a#wrefresh").removeClass('ui-disabled');
            $("a#wrefresh_h").removeClass('ui-disabled');
            console.log("Timer stoped - reason: user navigated away");
        }

        console.log("_startTracingWorkTarget() thread started");

        console.log("read my location now");

        navigator.geolocation.getCurrentPosition(function(position){

            console.log("my location found");

            var longitude = position.coords.longitude;
            var latitude = position.coords.latitude;
            var altitude = position.coords.altitude;

                                        //update location on map
                                        //Gsingle_track_marker_Mine

                if (typeof google == 'undefined') {
                    //quite
                }else{

                    location = new google.maps.LatLng(latitude, longitude);

                    if (Gsingle_track_marker_Mine == null) {

                        Gsingle_track_marker_Mine = new google.maps.Marker({
                            icon: {
                                path: google.maps.SymbolPath.CIRCLE,
                                scale: 10
                                },
                            position: location,
                            map: map,
                            title: 'Me!'
                        });

                    }else{

                        Gsingle_track_marker_Mine.setMap(null);
                        Gsingle_track_marker_Mine = null;

                        Gsingle_track_marker_Mine = new google.maps.Marker({
                            icon: {
                                path: google.maps.SymbolPath.CIRCLE,
                                scale: 10
                            },
                            position: location,
                            map: map,
                            title: 'Me!'
                        });
                    }

                    //console.log("Updating my marker position");
                    //Gsingle_track_marker.setMap(map);
                }

                
                setTimeout(function(){

                    if (typeof google == 'undefined') {
                        //silence
                    }else{
                        console.log("resizing the map - because of change in my location");
                        google.maps.event.trigger(map,'resize');
                        map.setCenter(Gsingle_track_marker_Mine.getPosition());
                    }
                                                
                }, 2000);

        }, onGPSTrackError, GPSoptions);
    };


    $(document).on("pagebeforecreate", "#eventMap", function(){
        updateEventMapDOM();
    });

    function updateEventMapDOM(){
        
        var traceid = $("input#codetotrace").val().trim();

        $.mobile.loading('show');
    
        $.ajax({
                type: 'GET',
                url: tracehost+'/event/getLocationDetails/' + traceid,
                data: null,
                success: function(data) {
                    
                        if(data.status){
                            
                            initializeMap(data.result,'einfoDirPanel', 'eventmap_canvas');

                            setTimeout(function(){
                                $.mobile.loading( 'hide', {});
                                setMapAtCenter();
                                $('#eeExtraInfo').html("<address><p><em>Trace Code: </em><b> "+formatTraceCode(data.result.traceid)+"</b></p>"+  "<p><em>Place:</em> "+data.result.nameofplace+"</p>"+ "<p><em>Event Type:</em> "+data.result.category+"</p>"+ "<p><em>Address:</em> "+data.result.address+"</p>"+ "<p><em>Nearby:</em> "+data.result.location_id.nearby+"</p>"+ "<p><em>City: "+data.result.city+ "<p><em>Note:</em> "+data.result.note+"</p>"+"</em></p></address>");
                            },2000);
                        }else{
                            //the delay is neccessary to properly display the popup
                            setTimeout(function(){
                                $.mobile.loading( 'hide', {});
                                $('#eventmap_canvas').remove();
                                showPopUp('The Trace Code does not exist or as expired.');
                            },2000);
                        }
                },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
        });
    }
    
    $(document).on("pageinit", "#eventMap", function(){
        
        $("#etrack").click(function(){

            navigator.geolocation.getCurrentPosition(onGPSTrackSuccess, onGPSTrackError, GPSoptions);
            $.mobile.loading('show', {});
        });
    
        $("#erefresh").click(function(){
            updateEventMapDOM();
        });
    
        $('#traceevent').click(function(){
            showPopUp('Not supported yet!');
        });
    });
    
    $(document).on("pageinit", "#mobilepageinit", function(){

        $('#refreshCheckRequest').click(function(){
            updateMobileDOMVisibility();
        });


        $('#refreshPendingRequest').click(function(){
            updateMobileDOMPendingRequest();
        });

        $('#refreshMobileInfo').click(function(){
            updateMobileDOMInfo();
        });
    
        $("#mobileDestroyer").click(function(){
            destroyMobileTraces();
        });

        $("#mobileDestroyer_h").click(function(){
            destroyMobileTraces();
        });
    
        $('#seeTargetonMap').click(function(){
    
            if (mobileGlobalTracid != 'undefined' || mobileGlobalTracid != null) {
                $.mobile.changePage("#mobileMap",{});
            }else{
                showPopUp('Oopse! an error occured, please hit refresh');
            }
        });
    
        
        $("#seeTargetsonMap").click(function(){ //note the plural
            displayAllTargetsOnMap(true);
        });

        $("#seeTargetsonMap_h").click(function(){ //note the plural
            displayAllTargetsOnMap(true);
        });
    
    });

    function displayAllTargetsOnMap(flag){
            $.ajax({
                    type: 'GET',
                    url: tracehost+'/mobile/canSeeTargetsOnMultiMap/',
                    data: null,
                    success: function(data) {
                            if(data.status){
                                if(flag){
                                    $.mobile.changePage("#mobilemultiMap",{});
                                }else{
                                    $("a#seeTargetsonMap").removeClass('ui-disabled');
                                    $("a#seeTargetsonMap_h").removeClass('ui-disabled');
                                }

                            }else{
                                if(flag)
                                    showPopUp('Access denied! To access targets, create a mobile trace code and share.');
                            }
                    },
                    error: ajaxConnectionError,
                    contentType: "application/json",
                    dataType: 'json'
            });
    }

    function destroyMobileTraces(){

        $.ajax({
                    type: 'GET',
                    url: tracehost+'/mobile/destroyMe/',
                    data: null,
                    success: function(data) {
                            if(data.status){
                                //$("a#mobileDestroyer").attr('disabled', 'disabled');
                                $("a#mobileDestroyer").addClass('ui-disabled');
                                $("a#mobileDestroyer_h").addClass('ui-disabled');
                                //$("a#mobileBtn").attr('href', "#mobilePage");//already on html page
                                $("a#mobileBtn").removeClass('ui-disabled');
                                $("a#mobileBtn_h").removeClass('ui-disabled');
                                $('#mExtraInfo').html("<address><p><em>Trace Code: </em><b>"+formatTraceCode("0000000000000000")+"</p></address>");
                                showPopUp('Mobile details destroyed');

                                if (deviceSupportsLocalStorage()) {
                                    localStorage.setItem("share.traceid", null);
                                }

                            }else{
                                showPopUp('Oopse! Server said: ' + data.reason);
                            }
                    },
                    error: ajaxConnectionError,
                    contentType: "application/json",
                    dataType: 'json'
        });
    }

    
    $(document).on("pagebeforecreate", "#mobilepageinit", function(){
        updateMobileDOMInfo();
        updateMobileDOMVisibility();
        updateMobileDOMPendingRequest();
    });

    function updateMobileDOMPendingRequest(){

        //console.log("pending request Fired!!")

        $.mobile.loading('show');

        $.ajax({
                type: 'GET',
                url: tracehost+'/mobile/getPendingRequest',
                data: null,
                success: function(data) {
    
                        if(data.status){
                            if(data.result.authorized == '0'){
                                $('#mPendingRequest').html("Sorry! your request to the trace code <b>"+ formatTraceCode(data.result.traceid_b) + "</b> is still pending");
                            }else if(data.result.authorized == '1'){
                                $('#mPendingRequest').html("Congratulations! your request to the trace code <b>"+ formatTraceCode(data.result.traceid_b) + "</b> was accepted");
                                $.mobile.loading('hide');
                                $('#seeTargetonMap').show();
                                mobileGlobalTracid = data.result.traceid_b;

                                if (deviceSupportsLocalStorage()) {
                                    localStorage.setItem("global.traceid", data.result.traceid_b); //not used TODO
                                }

                            }else if(data.result.authorized == '2'){
                                $('#mPendingRequest').html("Sorry! your request to the trace code <b>"+ formatTraceCode(data.result.traceid_b) + "</b> was rejected");
                            }else{
                                $('#mPendingRequest').html("Sorry! your request to the trace code <b>"+ formatTraceCode(data.result.traceid_b) + "</b> has expired");
                            }
                        }

                        $.mobile.loading('hide');
                },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
        });
    }


    function updateMobileDOMVisibility(){

        //console.log("visibility request fired!!")

        $('#authorizeIt').hide();

        $.mobile.loading('show');
        
        $.ajax({
                type: 'GET',
                url: tracehost+'/mobile/getRequest',
                data: null,
                success: function(data) {
    
                        if(data.status){
                            //only one record supported for now. using findOne wrapper
                            //for (var i = data.result.length - 1; i >= 0; i--) {
                            //    data.result[i]
                            //};
                            if (data.result.length != 0) {
                                $('#authorizeIt').show();
                                var func = 'AuthorizeMobileRequest("'+data.result[0]._id+'"); return false;';
                                $('#authorizeDynBtn').attr('onClick', func);
                                $('#mRequestInfo').html("You have a request from " + data.result[0].nickname);
                                $('#refreshCheckRequest').hide();
                            };
                            //
                            //
                            
                        }else{
                             //$('#mRequestInfo').html("");
                        }

                        $.mobile.loading('hide');
                },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
        });
    }

    function updateMobileDOMInfo(){

        //console.log("info box refresh fired!!!!!")

        $.mobile.loading('show');
    
        $.ajax({
                type: 'GET',
                url: tracehost+'/mobile/canCreate/',
                data: null,
                success: function(data) {
    
                        if(data.status){
                            //$("a#mobileBtn").attr('href', '#');
                            $("a#mobileBtn").addClass('ui-disabled');
                            $("a#mobileBtn_h").addClass('ui-disabled');
                            $("a#mobileDestroyer").removeClass('ui-disabled');
                            $("a#mobileDestroyer_h").removeClass('ui-disabled');
                            console.log("enabling mobileDestroyer, disabling generate");
                            //var func = 'showPopUp("Destroy your active trace code first."); return false';
                            //$("a#mobileBtn").attr('onClick', func);
                            $('#mExtraInfo').html("<address><p><em>Trace Code: </em><b> "+formatTraceCode(data.traceid)+"</p></address>");
                            //$('#mtracenumber').html(data.traceid);
                            if (deviceSupportsLocalStorage()) {
                                    localStorage.setItem("share.traceid", data.traceid);
                            }

                            var content = data.traceid;
                            // var qrcode = "https://chart.googleapis.com/chart?cht=qr&chs="+QRsize+"&chl="+content+"&choe="+QRencoding+"&chld="+QRcorrection;
                            // $("img#mobileqrcode").attr('src', qrcode);

                            showQRCode('mobile', content);
                            displayAllTargetsOnMap(false);
                        }else{
                            console.log("Error: "+data.reason);
                            $('#mExtraInfo').html("<address><p><em>Trace Code: </em><b>"+formatTraceCode("0000000000000000")+"</p></address>");
                            $("a#mobileBtn").removeClass('ui-disabled');
                            $("a#mobileBtn_h").removeClass('ui-disabled');
                            $("a#mobileDestroyer").addClass('ui-disabled');
                            $("a#mobileDestroyer_h").addClass('ui-disabled');
                            $("a#seeTargetsonMap").addClass('ui-disabled');
                            $("a#seeTargetsonMap_h").addClass('ui-disabled');
                        }

                        $.mobile.loading('hide');
                },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
        });
    }
    
    $(document).on("pageinit", "#eventpageinit", function(){

        $("#refreshEventDOM").click(function(){
            updateEventDOM();
        });

        $("#eventDestroyer").click(function(){
    
            $.mobile.loading('show', {});
    
            $.ajax({
                    type: 'GET',
                    url: tracehost+'/event/destroyMe/',
                    data: null,
                    success: function(data) {
                            if(data.status){
                                //$("a#eventDestroyer").attr('disabled', 'disabled');
                                $("a#eventDestroyer").addClass('ui-disabled');
                                $("a#eventBtn").attr('href', "#eventPage");
                                showPopUp('Event details destroyed');

                                if (deviceSupportsLocalStorage()) {
                                        localStorage.setItem("share.traceid", null);
                                        localStorage.setItem("user.events.cached", 'false');// tell eventpage to update cache with internet materials
                                }

                            }else{
                                showPopUp('Oopse! Server said: ' + data.reason);
                            }

                            $.mobile.loading('hide');
                    },
                    error: ajaxConnectionError,
                    contentType: "application/json",
                    dataType: 'json'
            });
        });
    
    });
    
    $(document).on("pagebeforecreate", "#eventpageinit", function(){
       updateEventDOM();
    });

    function updateEventDOM(){

        console.log("EventDOM fired!!!");

        $.mobile.loading('show', {});


        if (deviceSupportsLocalStorage()) {
            if (localStorage.getItem("user.events.cached") == 'true') {
                showQRCode('event', localStorage.getItem('user.events.traceid'));
                localStorage.setItem("share.traceid", localStorage.getItem('user.events.traceid'));
                $('#eExtraInfo').html("<address><p><em>Trace Code: </em><b> "+ formatTraceCode(localStorage.getItem('user.events.traceid')) +"</b></p>"+  "<p><em>Place:</em> "+localStorage.getItem('user.events.nameofplace')+"</p>"+ "<p><em>Event Type:</em> "+localStorage.getItem('user.events.category')+"</p>"+ "<p><em>Address:</em> "+localStorage.getItem('user.events.address')+"</p>"+ "<p><em>Nearby:</em> "+localStorage.getItem('user.events.nearby')+"</p>"+ "<p><em>City:</em> "+localStorage.getItem('user.events.city')+ "<p><em>Note:</em> "+localStorage.getItem('user.events.note')+"</p>"+"</p></address>");
                $.mobile.loading('hide');
                return;
            }
        }
    
        $.ajax({
                type: 'GET',
                url: tracehost+'/event/getActiveEvent/',
                data: null,
                success: function(data) {
    
                        if(data.status){
    
                            $("a#eventBtn").attr('href', '#');
                            
                                $.ajax({
                                    type: 'GET',
                                    url: tracehost+'/event/getLocationDetails/' + data.result.traceid,
                                    data: null,
                                    success: function(data) {

                                            $.mobile.loading( 'hide', {});

                                            if(data.status){
                                                $('#eExtraInfo').html("<address><p><em>Trace Code: </em><b> "+formatTraceCode(data.result.traceid)+"</b></p>"+  "<p><em>Place:</em> "+data.result.nameofplace+"</p>"+ "<p><em>Event Type:</em> "+data.result.category+"</p>"+ "<p><em>Address:</em> "+data.result.address+"</p>"+ "<p><em>Nearby:</em> "+data.result.location_id.nearby+"</p>"+ "<p><em>City:</em> "+data.result.city+ "<p><em>Note:</em> "+data.result.note+"</p>"+"</p></address>");
                                                if (deviceSupportsLocalStorage()) {

                                                    localStorage.setItem("share.traceid", data.result.traceid);
                                                    localStorage.setItem("user.events.cached", 'true');//make false after use
                                                    localStorage.setItem("user.events.traceid", data.result.traceid);
                                                    localStorage.setItem("user.events.address", data.result.address);
                                                    localStorage.setItem("user.events.nearby", data.result.location_id.nearby);
                                                    localStorage.setItem("user.events.city", data.result.city);
                                                    localStorage.setItem("user.events.nameofplace", data.result.nameofplace);
                                                    localStorage.setItem("user.events.category", data.result.category);
                                                    localStorage.setItem("user.events.note", data.result.note);
                                                }

                                                var content = data.result.traceid;
                                                // var qrcode = "https://chart.googleapis.com/chart?cht=qr&chs="+QRsize+"&chl="+content+"&choe="+QRencoding+"&chld="+QRcorrection;
                                                // $("img#eventqrcode").attr('src', qrcode);

                                                showQRCode('event', content);

                                            }else{
                                                showPopUp('The Trace Code does not exist or as expired.');
                                            }
                                    },
                                    error: ajaxConnectionError,
                                    contentType: "application/json",
                                    dataType: 'json'
                            });
                        }else{
                            $.mobile.loading( 'hide', {});
                            $('#eExtraInfo').html("<address><p><em>Trace Code: </em><b>"+formatTraceCode("0000000000000000")+"</b></p>"+"</address>");
                        }
                },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
        });
    }


    $(document).on("pagebeforecreate", "#mobileMap", function(){
        //call a function here because this event is only fired ones, add a refresh button to call manual the second time
        updateMobleMapDOM();
    });

    function updateMobleMapDOM(){

        //console.log("UpdatteMobileMap Fired!!!");

        var traceid;
    
        if (mobileGlobalTracid != null || mobileGlobalTracid != 'undefined') {
            traceid = mobileGlobalTracid;
        }else{
            traceid = $("input#codetotrace").val().trim();
            mobileGlobalTracid = traceid;
        }

        $.mobile.loading('show');
    
        $.ajax({
                type: 'GET',
                url: tracehost+'/mobile/getBLocationDetails/' + traceid,
                data: null,
                success: function(data) {
                    
                        if(data.status){
                            $.mobile.loading( 'hide', {});
                            initializeMap(data.result.location_id_b,'minfoDirPanel','mobilemap_canvas');
                            setTimeout(function(){
                                setMapAtCenter();
                                $('#mmExtraInfo').html("<address><p><em>Trace Code: </em><b> "+formatTraceCode(data.result.traceid_b) +"</b></p>"+ "</address>");
                            },2000);
                        }else{
                            //the delay is neccessary to properly display the popup
                            setTimeout(function(){
                                $.mobile.loading( 'hide', {});
                                $('#mobilemap_canvas').remove();
                                showPopUp('The Trace Code does not exist or as expired.');
                            },2000);
                        }
                },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
        });

    }
    

    $(document).on("pageinit", "#mobileMap", function(){
    
        $("#mtrack").click(function(){
            navigator.geolocation.getCurrentPosition(onGPSTrackSuccess, onGPSTrackError, GPSoptions);
            $.mobile.loading('show', {});
        });
    
        $("#mrefresh").click(function(){
            updateMobleMapDOM();
        });
    
        $('#tracesinglemap').click(function(){
            startTracingTarget();
        });

        $("#mtrack_h").click(function(){
            navigator.geolocation.getCurrentPosition(onGPSTrackSuccess, onGPSTrackError, GPSoptions);
            $.mobile.loading('show', {});
        });
    
        $("#mrefresh_h").click(function(){
            updateMobleMapDOM();
        });
    
        $('#tracesinglemap_h').click(function(){
            startTracingTarget();
        });
    });


    function startTracingTarget(){
        //showPopUp('Not supported yet!');
        //TODO: disable the trace icon here then remove the return
        //return;
        //see who invited you: http://localhost:3000/mobile/getBLocationDetails/:traceid
        //traceid is store in global var:= mobileGlobalTracid
        //1: setup the custom marker
        //2: create the thread here with marker object as parameter
        //3: whenever the page title doesnt match #mobileMap (user navigated away) then destroy thread
        $("a#mtrack").addClass('ui-disabled'); //vertical display
        $("a#mtrack_h").addClass('ui-disabled'); //horizontal display
        $("a#tracesinglemap").addClass('ui-disabled');
        $("a#tracesinglemap_h").addClass('ui-disabled');
        $("a#mrefresh").addClass('ui-disabled');
        $("a#mrefresh_h").addClass('ui-disabled');
        _startTracingTarget();
        //console.log("tracing target")
        mthread_id1 = setInterval(_startTracingTarget, 1000 * 10); //30 seconds interval
        startSendingMyCurrentLocation();
        //console.log("sending my current location in another thread");
        mthread_id2 = setInterval(startSendingMyCurrentLocation, 1000 * 10); //30 seconds interval
    }


    function startSendingMyCurrentLocation(){

        //update location on map using custom icon
        //send location to server POST: /mobile/updateLocation 
        //params: {altitude, latitude, longitude}

        console.log("startSendingMyCurrentLocation() thread started");

        console.log("read my location now");

        navigator.geolocation.getCurrentPosition(function(position){

            console.log("my location found");

            var longitude = position.coords.longitude;
            var latitude = position.coords.latitude;
            var altitude = position.coords.altitude;

            console.log("sending my location to server");

                    $.ajax({
                        type: 'POST',
                        url: tracehost + '/mobile/updateLocation',
                        data: JSON.stringify({longitude: longitude, latitude: latitude, altitude: altitude}),
                        success: function(data) {
                                    if(data.status){
                                        console.log("updated my GPS cordinate, setting updating marker position");
                                        //update location on map
                                        //Gsingle_track_marker_Mine

                                        if (typeof google == 'undefined') {

                                        }else{

                                        var location = new google.maps.LatLng(latitude, longitude);

                                        if (Gsingle_track_marker_Mine == null) {

                                            Gsingle_track_marker_Mine = new google.maps.Marker({
                                                icon: {
                                                    path: google.maps.SymbolPath.CIRCLE,
                                                    scale: 10
                                                },
                                                position: location,
                                                map: map,
                                                title: 'Me!'
                                            });

                                        }else{

                                            Gsingle_track_marker_Mine.setMap(null);
                                            Gsingle_track_marker_Mine = null;

                                            Gsingle_track_marker_Mine = new google.maps.Marker({
                                              icon: {
                                                    path: google.maps.SymbolPath.CIRCLE,
                                                    scale: 10
                                                },
                                                position: location,
                                                map: map,
                                                title: 'Me!'
                                            });
                                        }

                                        //console.log("Updating my market position");
                                        //Gsingle_track_marker.setMap(map);
                                    }

                                        setTimeout(function(){

                                                if (typeof google == 'undefined') {
                                                    //silence
                                                }else{
                                                    console.log("resizing the map - because of change in my location");
                                                    google.maps.event.trigger(map,'resize');
                                                    map.setCenter(Gsingle_track_marker_Mine.getPosition());
                                                }
                                                
                                        }, 2000);
                                    }else{
                                        console.log("an error occured, updating GPS cordinate");
                                    }
                                },
                        error: ajaxConnectionError,
                        contentType: "application/json",
                        dataType: 'json'
                    });

        }, onGPSTrackError, GPSoptions);
    };


    function _startTracingTarget(){

        console.log("_startTracingTarget thread runing");

        var page = $(':mobile-pagecontainer').pagecontainer('getActivePage')[0].id;
        //var page = "mobileMap";

        if (page != "mobileMap") {
            //kill this thread, and enable trace button, since user has navigated away            
            clearInterval(mthread_id1);
            clearInterval(mthread_id2);
            //TODO: Enable them back
            $("a#mtrack").removeClass('ui-disabled'); //vertical display
            $("a#mtrack_h").removeClass('ui-disabled'); //horizontal display
            $("a#tracesinglemap").removeClass('ui-disabled');
            $("a#tracesinglemap_h").removeClass('ui-disabled');
            $("a#mrefresh").removeClass('ui-disabled');
            $("a#mrefresh_h").removeClass('ui-disabled');
            console.log("Timer stoped - reason: user navigated away");
        }


        $.ajax({
                type: 'GET',
                url: tracehost+'/mobile/getBLocationDetails/' + mobileGlobalTracid,
                data: null,
                success: function(data) {
                    
                        if(data.status){

                            console.log("got location of target: lat: " + data.result.location_id_b.gps.latitude + " long: " + data.result.location_id_b.gps.longitude);

                            if (typeof google == 'undefined') {

                            }else{
                            var location = new google.maps.LatLng(data.result.location_id_b.gps.latitude, data.result.location_id_b.gps.longitude);

                            if (Gsingle_track_marker == null) {

                                Gsingle_track_marker = new google.maps.Marker({
                                    icon: {
                                        path: google.maps.SymbolPath.CIRCLE,
                                        scale: 10
                                    },
                                    position: location,
                                    map: map,
                                    title: 'Target!'
                                });

                            }else{

                                Gsingle_track_marker.setMap(null);
                                Gsingle_track_marker = null;

                                Gsingle_track_marker = new google.maps.Marker({
                                  icon: {
                                        path: google.maps.SymbolPath.CIRCLE,
                                        scale: 10
                                    },
                                    position: location,
                                    map: map,
                                    title: 'Target!'
                                });
                            }

                            //console.log("adding marker for target");
                            //already added hence totoulogy
                            //Gsingle_track_marker.setMap(map);
                        }
                            setTimeout(function(){

                                    if (typeof google == 'undefined') {
                                        //silence
                                    }else{
                                        console.log("resizing map now, to reflect updated marker");
                                        google.maps.event.trigger(map,'resize');
                                        map.setCenter(Gsingle_track_marker.getPosition());
                                    }
                                    
                            }, 2000);

                        }else{
                            //Trace code destroy or doesnt exists
                        }
                },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
        });

        
    }
    



    $(document).on("pagebeforecreate", "#mobilemultiMap", function(){
        //call a function here because this event is only fired ones, add a refresh button to call manual the second time
        updateMobleMultiMapDOM();
    });


    function updateMobleMultiMapDOM(){

        console.log("update mobile multi map dom fired!!!");

        $.mobile.loading('show');

        $.ajax({
                type: 'GET',
                url: tracehost+'/mobile/getAllAuthorizedLocationDetails/',
                data: null,
                success: function(data) {
                    
                        if(data.status){
                            
                            initializeMultiMap(data.result,'mmultiinfoDirPanel','multimapmap_canvas');

                            setTimeout(function(){
                                $.mobile.loading( 'hide', {});
                                setMapAtCenter();
                                //$('#mmmultiExtraInfo').html("<address><p><em>Trace Code:</em><b> "+data.result.traceid_b+"</b></p>"+ "</address>");
                                //perform a loop here
                            },2000);

                        }else{
                            //the delay is neccessary to properly display the popup
                            setTimeout(function(){
                                $.mobile.loading( 'hide', {});
                                $('#multimapmap_canvas').remove();
                                showPopUp('The Trace Code does not exist or as expired.');
                            },2000);
                        }
                },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
        });


    }


    
    $(document).on("pageinit", "#mobilemultiMap", function(){
    
        $('#tracemultimap').click(function(){
           startTracingMultipleTargets();
        });
    
    
        $("#mmultitrack").click(function(){
            showPopUp('Not supported yet!');
        });
    
        $("#mmrefresh").click(function(){
            updateMobleMultiMapDOM();
        });

        $('#tracemultimap_h').click(function(){
           startTracingMultipleTargets();
        });
    
    
        $("#mmultitrack_h").click(function(){
            showPopUp('Not supported yet!');
        });
    
        $("#mmrefresh_h").click(function(){
            updateMobleMultiMapDOM();
        });
    });


    function startTracingMultipleTargets(){
        //TODO: disable the trace icon here then remove the return
        //traceid is store in global var:= mobileGlobalTracid TODO: tobe used in computing distance
        //1: setup the custom marker
        //2: create the thread here with marker object as parameter
        //3: whenever the page title doesnt match #traceMap (user navigated away) then destroy thread
        //disable Trace 'n' Trace buttons, including refresh
        $("a#mmultitrack").addClass('ui-disabled'); //vertical display
        $("a#mmultitrack_h").addClass('ui-disabled'); //horizontal display
        $("a#tracemultimap").addClass('ui-disabled');
        $("a#tracemultimap_h").addClass('ui-disabled');
        $("a#mmrefresh").addClass('ui-disabled');
        $("a#mmrefresh_h").addClass('ui-disabled');
        _startTracingMultipleTargets();
        mmthread_id_slaves = setInterval(_startTracingMultipleTargets, 1000 * 10); //30 seconds interval
        mmthread_id_master = setInterval(_startReadingMastersLocation, 1000 * 10); //30 seconds interval
    }

    function _startTracingMultipleTargets(){

        $.ajax({
                type: 'GET',
                url: tracehost+'/mobile/getAllAuthorizedLocationDetails/',
                data: null,
                success: function(data) {
                    
                        if(data.status){
                            placeMulipleMarkersOnMap(data.result)                           
                        }else{
                            //the delay is neccessary to properly display the popup
                            setTimeout(function(){
                                $.mobile.loading( 'hide', {});
                                $('#multimapmap_canvas').remove();
                                showPopUp('The Trace Code does not exist or as expired.');
                            },2000);
                        }
                },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
        });
    }


    function _startReadingMastersLocation(){

        //update location on map using custom icon
        //send location to server POST: /mobile/updateLocation 
        //params: {altitude, latitude, longitude}

         var page = $(':mobile-pagecontainer').pagecontainer('getActivePage')[0].id;
        //var page = "mobileMap";

        if (page != "mobilemultiMap") {
            //kill this thread, and enable trace button, since user has navigated away            
            clearInterval(mmthread_id_slaves);
            clearInterval(mmthread_id_master);
            //TODO: Enable them here
            $("a#mmultitrack").removeClass('ui-disabled'); //vertical display
            $("a#mmultitrack_h").removeClass('ui-disabled'); //horizontal display
            $("a#tracemultimap").removeClass('ui-disabled');
            $("a#tracemultimap_h").removeClass('ui-disabled');
            $("a#mmrefresh").removeClass('ui-disabled');
            $("a#mmrefresh_h").removeClass('ui-disabled');
            console.log("Timer stoped - reason: user navigated away");
        }

        console.log("__startTracingMultipleTargets() thread started");

        console.log("read my location now");

        navigator.geolocation.getCurrentPosition(function(position){

            console.log("my location found");

            var longitude = position.coords.longitude;
            var latitude = position.coords.latitude;
            var altitude = position.coords.altitude;

            console.log("sending my location to server");

                    $.ajax({
                        type: 'POST',
                        url: tracehost + '/mobile/updateLocation',
                        data: JSON.stringify({longitude: longitude, latitude: latitude, altitude: altitude}),
                        success: function(data) {
                                    if(data.status){
                                        console.log("updated my GPS cordinate, setting updating marker position");
                                        //update location on map
                                        //Gsingle_track_marker_Mine

                                        if (typeof google == 'undefined') {

                                        }else{

                                        var location = new google.maps.LatLng(latitude, longitude);

                                        if (Gsingle_track_marker_Mine == null) {

                                            Gsingle_track_marker_Mine = new google.maps.Marker({
                                                icon: {
                                                    path: google.maps.SymbolPath.CIRCLE,
                                                    scale: 10
                                                },
                                                position: location,
                                                map: map,
                                                title: 'Me!'
                                            });

                                        }else{

                                            Gsingle_track_marker_Mine.setMap(null);
                                            Gsingle_track_marker_Mine = null;

                                            Gsingle_track_marker_Mine = new google.maps.Marker({
                                              icon: {
                                                    path: google.maps.SymbolPath.CIRCLE,
                                                    scale: 10
                                                },
                                                position: location,
                                                map: map,
                                                title: 'Me!'
                                            });
                                        }

                                        //console.log("Updating my market position");
                                        //Gsingle_track_marker.setMap(map);
                                    }

                                        setTimeout(function(){

                                                if (typeof google == 'undefined') {
                                                    //silence
                                                }else{
                                                    console.log("resizing the map - because of change in my location");
                                                    google.maps.event.trigger(map,'resize');
                                                    map.setCenter(Gsingle_track_marker_Mine.getPosition());
                                                }
                                                
                                        }, 2000);
                                    }else{
                                        console.log("an error occured, updating GPS cordinate");
                                    }
                                },
                        error: ajaxConnectionError,
                        contentType: "application/json",
                        dataType: 'json'
                    });

        }, onGPSTrackError, GPSoptions);
    };



    function placeMulipleMarkersOnMap(locationDataArray){
        //setup the global variable to be used later
        if(locationDataArray.length == 0 && (typeof google == 'undefined'))
            return false;

        for (var i = 0; i < markers.length; i++)
            markers.setMap(null);
        markers.length = 0; //delete all references to the markers
        markers = Array(locationDataArray.length);
    
        for (var i = locationDataArray.length - 1; i >= 0; i--) {
    
            markers[i] = new google.maps.Marker({
              position: new google.maps.LatLng(locationDataArray[i].location_id_a.gps.latitude, locationDataArray[i].location_id_a.gps.longitude),
              title: locationDataArray[i].nickname + " [" + locationDataArray[i].location_id_a.lastupdated + "]"
            });

            markers[i].setMap(map);
        };

        setTimeout(function(){
            map.setCenter(markers[0].getPosition());
        },2000);
    }
    
    $(document).on("pageinit", "#tracePage", function(){

        $("#traceDemo").click(function(){

            if (deviceSupportsLocalStorage()) {
                localStorage.setItem("global.traceid", TESTCODE); //make false after use
                $("input#codetotrace").attr('value', TESTCODE);
                $.mobile.changePage("#traceMap",{});
            }else{                                
                $("input#codetotrace").attr('value', TESTCODE);
            }

        });

        $("#traceNow").click(function(){

            if (deviceSupportsLocalStorage()) {
                localStorage.setItem("global.traceid", null); //make false after use
            }
    
            var traceid = $("input#codetotrace").val().trim();
    
            if (traceid.length < 5) {
                showPopUp('Please enter the Trace code');
                return false;
            };
    
            $.mobile.loading('show', {});
    
            $.ajax({
                type: 'GET',
                url: tracehost+'/universe/getLocationType/' + traceid,
                data: null,
                success: function(data) {
    
                            if(data.status){

                                if (deviceSupportsLocalStorage()) {
                                    localStorage.setItem("global.traceid", traceid); //make false after use
                                }

                                //1. initialize map here
                                //2. show basic google map here with marker on destination,
                                //3. Do 2. if trace code is home or workplace or event... for mobile location, we must read user's coordinate
                                //because we want the targets to see who ever is coming to them
                                //4. set up buttons on stage and menu for user (Trace, Moving mode, guidience etc)

                                if (data.result == 'home') {
                                    //redirect to homemap (Default)
                                    $.mobile.changePage("#traceMap",{});
                                }else if(data.result == 'work'){
                                    //redirect to workmap
                                    $.mobile.changePage("#workMap",{});
                                }else if(data.result == 'mobile'){
                                    //redirect to mobile - The most sensitive
                                                            
                                        var reqstatus = data.reason;
    
                                        if(reqstatus == "cantmakerequest"){
                                            $.mobile.loading( 'hide', {});
                                            showPopUp('Please, create your mobile trace code first before making a request');
                                        }else if(reqstatus == "norequest"){
                                            $.mobile.changePage("#mobileRequest",{});
                                        }else if(reqstatus == "pending"){
                                            $.mobile.loading( 'hide', {});
                                            showPopUp('Sorry, your request is still pending authorization');
                                        }else if(reqstatus == "rejected"){
                                            $.mobile.loading( 'hide', {});
                                            showPopUp('Sorry, your request has been rejected by target');
                                        }else if(reqstatus == "accepted"){
                                            $.mobile.changePage("#mobileMap",{});
                                        }else{
                                            $.mobile.loading( 'hide', {});
                                            showPopUp('Sorry, The Trace code has expired or does not exist');
                                        }
    
                                }else if(data.result == 'events'){
                                    console.log("here");
                                    $.mobile.changePage("#eventMap",{});
                                }else{
                                    $.mobile.loading( 'hide', {});
                                    showPopUp('A strange trace code was entered');
                                }
                            }else{
                                $.mobile.loading( 'hide', {});
                                showPopUp('The Trace code is invalid or destroyed');
                                return false;
                            }
                        },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
            });
        });
    });
    
    
    $(document).on("pageinit", "#workgeo", function(){

        $("#readWorkLocation").click(function(){
            navigator.geolocation.getCurrentPosition(submitWorkCoordinateSuccess, onGPSTrackError, GPSoptions);
            $.mobile.loading('show', {});
        });
    });
    
    
    function submitWorkCoordinateSuccess(position){
        //call cordova here for gps coordinates
                var longitude = position.coords.longitude;
                var latitude = position.coords.latitude;
                var altitude = position.coords.altitude;
    
                if (latitude.length == 0 || longitude.length == 0) {
                    $.mobile.loading( 'hide', {});
                    showPopUp('No coordinate available, Please try again');
                    return false;
                }


                if (deviceSupportsLocalStorage()) {
                    localStorage.setItem("reg.work.isCreated", 'true'); //make false after use
                    localStorage.setItem("reg.work.longitude", longitude);
                    localStorage.setItem("reg.work.latitude", latitude);
                    localStorage.setItem("reg.work.altitude", altitude);
                    $.mobile.loading('hide');

                    showPopUp('Your location has been read.');
                    setTimeout(function(){
                        $.mobile.changePage("#workform",{});
                    },2000);

                }else{
    
                    $.ajax({
                        type: 'POST',
                        url: tracehost + '/user/workplacegeo',
                        data: JSON.stringify({longitude: longitude, latitude: latitude, altitude: altitude}),
                        success: function(data) {
                                    if(data.status){
                                        $.mobile.loading('hide');
                                        showPopUp('Your location has been read.');
                                        setTimeout(function(){
                                            $.mobile.changePage("#workform",{});
                                        },2000);
                                        
                                        return true;
                                    }else{
                                        //alert("invalid usercode or passcode");
                                        $.mobile.loading('hide');
                                        showPopUp('Oopse! No GPS coordinates available');
                                        return false;
                                    }
                                },
                        error: ajaxConnectionError,
                        contentType: "application/json",
                        dataType: 'json'
                    });
                }
    }
    
    $(document).on("pageinit", "#workresult", function(){

        $("#gpsorcongrat").click(function(){
    
            $.mobile.loading('show', {});
    
            var resultid = $('input[name="radiowork"]').val();
    
            resultid = 0;
    
            var url;
    
            if(resultid == 0){
                return true;
            }else{
                //call remote server - End of workplace registration
            }
            return false;
        });
    });
    
    
    $(document).on("pagebeforecreate", "#work", function(){
        updateWorkDOM();
    });

    function updateWorkDOM(){

        //console.log("workdomrefresh fired!!");

        $.mobile.loading('show');

        if (deviceSupportsLocalStorage()) {
            if (localStorage.getItem("user.work.cached") == 'true') {
                showQRCode('work', localStorage.getItem('user.work.traceid'));
                localStorage.setItem("share.traceid", localStorage.getItem('user.work.traceid'));
                $('#wExtraInfo').html("<address><p><em>Title:</em><b> "+localStorage.getItem('user.work.workname')+"</b></p>" + "<p><em>Trace Code: </em> <b>"+ formatTraceCode(localStorage.getItem('user.work.traceid')) +"</b></p>" + "<p><em>Address:</em> "+localStorage.getItem('user.work.address')+"</p>"+ "<p><em>Tag:</em> "+localStorage.getItem('user.work.worktag')+"</p>"+ "<p><em>City:</em> "+localStorage.getItem('user.work.city')+"</p></address>");
                $.mobile.loading('hide');
                return;
            }
        }

        $.ajax({
                type: 'GET',
                url: tracehost+'/work/getMyTraceCode/',
                data: null,
                success: function(data) {
                        if(data.status){
                            
                                $.ajax({
                                    type: 'GET',
                                    url: tracehost+'/work/getLocationDetails/' + data.result,
                                    data: null,
                                    success: function(data) {
    
                                            $.mobile.loading( 'hide', {});
    
                                            if(data.status){

                                                $('#wExtraInfo').html("<address><p><em>Title:</em><b> "+data.result.workname+"</b></p>" + "<p><em>Trace Code: </em> <b>"+formatTraceCode(data.result.traceid) +"</b></p>" + "<p><em>Address:</em> "+data.result.address+"</p>"+ "<p><em>Tag:</em> "+data.result.worktag+"</p>"+ "<p><em>City:</em> "+data.result.city+"</p></address>");

                                                if (deviceSupportsLocalStorage()) {

                                                    localStorage.setItem("share.traceid", data.result.traceid);
                                                    localStorage.setItem("user.work.cached", 'true');//make false after use
                                                    localStorage.setItem("user.work.traceid", data.result.traceid);
                                                    localStorage.setItem("user.work.address", data.result.address);
                                                    localStorage.setItem("user.work.workname", data.result.workname);
                                                    localStorage.setItem("user.work.city", data.result.city);
                                                    localStorage.setItem("user.work.worktag", data.result.worktag)
                                                }

                                                var content = data.result.traceid;

                                                // var qrcode = "https://chart.googleapis.com/chart?cht=qr&chs="+QRsize+"&chl="+content+"&choe="+QRencoding+"&chld="+QRcorrection;
                                                // $("img#workqrcode").attr('src', qrcode);

                                                showQRCode('work', content);

                                            }else{
                                                showPopUp('Update your info to get a trace code');
                                                $('#wExtraInfo').html("<address><p><em>Trace Code: </em><b>"+formatTraceCode("0000000000000000")+"</b></p>"+"</address>");
                                            }
                                    },
                                    error: ajaxConnectionError,
                                    contentType: "application/json",
                                    dataType: 'json'
                            });
                        }else{
                            $.mobile.loading( 'hide', {});
                            $('#wExtraInfo').html("<address><p><em>Title:</em><b> "+formatTraceCode("0000000000000000")+"</b></p>"+"</address>");
                        }
                },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
        });
    }

    $(document).on("pageinit", "#work", function(){

        $("#refreshWorkDOM").click(function(){
            updateWorkDOM();
        });

        $("#refreshWorkDOM_h").click(function(){
            updateWorkDOM();
        });
    });
    
    $(document).on("pageinit", "#workPage", function(){

        $("#searchworkplace").click(function(){

            var workname = $("input#workname").val();
            var city = $("input#workcity").val();
    
            if(workname.length == 0 || city.length == 0){
                showPopUp('Entry is required');
                return false;
            }


            if (deviceSupportsLocalStorage()) {
                    localStorage.setItem("reg.work.isCreated", 'true');//make false after use
                    localStorage.setItem("reg.work.workname", workname);
                    localStorage.setItem("reg.work.city", city);
            }
            
    
            $.ajax({
                type: 'POST',
                url: tracehost + '/user/workplacesearch',
                data: JSON.stringify({workname: workname, city: city}),
                success: function(data) {
                            if(data.status){
                                $.mobile.changePage("#workresult",{});
                                return true;
                            }else{
                                $.mobile.loading( 'hide', {});
                                showPopUp('Error, Please try again later');
                                return false;
                            }
                        },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
            });

        }); 
    });
    
    $(document).on("pageinit", "#homeForm", function(){

        $("#createHome").click(function(){
    
            var address = $("textarea#homeaddress").val();
            var city = $("input#homecity").val();
            var nearby = $("input#homenearby").val();
    
            if(city.length == 0){
                showPopUp('City is a required field');
                return false;
            }
    
            $.mobile.loading('show', {});

            if (deviceSupportsLocalStorage()) {

                if (localStorage.getItem("reg.home.isCreated") != 'true') {

                    return false;
                }

                var longitude = localStorage.getItem("reg.home.longitude");
                var latitude = localStorage.getItem("reg.home.latitude");
                var altitude = localStorage.getItem("reg.home.altitude");

                

                localStorage.setItem("reg.home.isCreated", 'false');//make false after use

                $.ajax({
                    type: 'POST',
                    url: tracehost + '/home/updateDetailsOnce',
                    data: JSON.stringify({city: city, nearby: nearby, address: address, longitude: longitude, latitude: latitude, altitude: altitude}),
                    success: function(data) {
                                if(data.status){
        
                                    $.mobile.loading( 'hide', {});
                                    showPopUp('Your details have been saved');

                                    if (deviceSupportsLocalStorage()) {

                                        localStorage.setItem("user.home.cached", 'false');// tell homepage to update cache with internet materia
                                    }

                                    setTimeout(function(){
                                        $.mobile.changePage("#homePage",{});
                                    },3000);
        
                                    return;
                                }else{
                                    $.mobile.loading( 'hide', {});
                                    showPopUp('Error, Please try again later');
                                    return false;
                                }
                            },
                    error: ajaxConnectionError,
                    contentType: "application/json",
                    dataType: 'json'
                });

            }else{
    
                $.ajax({
                    type: 'POST',
                    url: tracehost + '/home/updateDetails',
                    data: JSON.stringify({city: city, nearby: nearby, address: address}),
                    success: function(data) {
                                if(data.status){
        
                                    $.mobile.loading( 'hide', {});
                                    showPopUp('Your details have been saved');
                                    setTimeout(function(){
                                        $.mobile.changePage("#homePage",{});
                                    },3000);
        
                                    return;
                                }else{
                                    $.mobile.loading( 'hide', {});
                                    showPopUp('Error, Please try again later');
                                    return false;
                                }
                            },
                    error: ajaxConnectionError,
                    contentType: "application/json",
                    dataType: 'json'
                });
            }
        }); 
    });
    
    
    $(document).on("pageinit", "#workform", function(){
    
        $("#createWork").click(function(){
    
            $.mobile.loading('show', {});
    
            var address = $("textarea#workaddress").val();
            var worktag = $("input#worktag").val();
            var nearby = $("input#worknearby").val();
    
    
            if(worktag.length == 0){
                $.mobile.loading( 'hide', {});
                showPopUp('Work Tag is compulsory');
                return false;
            }



            if (deviceSupportsLocalStorage()) {

                if (localStorage.getItem("reg.work.isCreated") != 'true') {
                    $.mobile.loading( 'hide', {});
                    showPopUp('Oopse! Please verify your entry and try again');
                    return false;
                }

                var workname = localStorage.getItem("reg.work.workname");
                var city = localStorage.getItem("reg.work.city");
                var longitude = localStorage.getItem("reg.work.longitude");
                var latitude = localStorage.getItem("reg.work.latitude");
                var altitude = localStorage.getItem("reg.work.altitude");


                $.ajax({
                    type: 'POST',
                    url: tracehost+'/work/updateWorkplace',
                    data: JSON.stringify({worktag: worktag, nearby: nearby, address: address, workname: workname, city: city, longitude: longitude, latitude: latitude, altitude: altitude}),
                    success: function(data) {
                                if(data.status){
                                    $.mobile.loading( 'hide', {});
                                    showPopUp('Success!');

                                    if (deviceSupportsLocalStorage()) {

                                        localStorage.setItem("user.work.cached", 'false');// tell homepage to update cache with internet materia
                                    }

                                    setTimeout(function(){
                                        $.mobile.changePage("#work",{});
                                    }, 3000);

                                }else{
                                    $.mobile.loading( 'hide', {});
                                    showPopUp('Oopse! Please verify your entry and try again. Traced returned: '+ data.reason);
                                }
                            },
                    error: ajaxConnectionError,
                    contentType: "application/json",
                    dataType: 'json'
                });
    
            }else{

                $.ajax({
                    type: 'POST',
                    url: tracehost+'/user/workplace',
                    data: JSON.stringify({worktag: worktag, nearby: nearby, address: address}),
                    success: function(data) {
                                if(data.status){
                                    $.mobile.loading( 'hide', {});
                                    showPopUp('Success!');
                                    setTimeout(function(){
                                        $.mobile.changePage("#work",{});
                                    }, 3000);
                                }else{
                                    $.mobile.loading( 'hide', {});
                                    showPopUp('Oopse! Please verify your entry and try again. Traced returned: '+ data.reason);
                                }
                            },
                    error: ajaxConnectionError,
                    contentType: "application/json",
                    dataType: 'json'
                });
            }

        });
     });
    
    
    $(document).on("pageinit", "#loginPage", function(){

        $("#authenticate").click(function(){
    
            var userCode= $("input#usercode").val();
            var passCode = $("input#passcode").val();
    
            $.mobile.loading('show', {});
    
            $.ajax({
                type: 'POST',
                crossDomain: true,
                url: tracehost+'/user/login',
                data: JSON.stringify({usercode: userCode, passcode: passCode}),
                success: function(data) {
                            if(data.status){
                                //initialize all that is needed through HMTL5 local or session storage before redirecting

                                if (deviceSupportsLocalStorage()) {

                                        localStorage.setItem("isLogedIn", 'true');
                                }

                                $.mobile.changePage("#profilePage",{});

                            }else{
                                $.mobile.loading( 'hide', {});
                                showPopUp('Username or password does not exist');
                                return false;
                            }
                        },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
            });
        });

    });
    

    $(document).on("pageinit", "#registerNewUserPage", function(){

        //

        $("#regUserNew").click(function(){
    
            //disable the register button
    
            var userCode= $("input#rusercode").val().trim();
            var passCode = $("input#rpasscode").val().trim();
            var qanimal = $("input#qanimal").val().trim();
            var qmother = $("input#qmother").val().trim();
            var qspace = $("input#qspace").val().trim();
            // var security3 = $("input#security3").val().trim();
            // var security4 = $("input#security4").val().trim();
    
            if(userCode.length == 0 || passCode.length == 0 || qanimal.length == 0 || qmother.length == 0 || qspace.length == 0){
                showPopUp('Please fill in the required details - Everything!');
                return;
            }
    
            $.mobile.loading('show', {});
    
            $.ajax({
                type: 'POST',
                url: tracehost+'/user/registerUserNew',
                data: JSON.stringify({usercode: userCode, passcode: passCode, animal: qanimal, mother: qmother, space: qspace}),
                success: function(data) {
                            if(data.status){
                                $.mobile.loading( 'hide', {});
                                showPopUp('Thank you for registering. Please wait..');
                                setTimeout(function(){
                                    $.mobile.changePage("#loginPage",{});
                                }, 3000);
                            }else{
                                $.mobile.loading( 'hide', {});
                                showPopUp('Oopse! Username is already taken. Please verify your entry and try again. Traced returned: '+ data.reason)
                            }
                        },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
            });
        });
    
    });
    
    
    $(document).on("pageinit", "#settings", function(){
    
    /*
     * Logout Handler
     */
        $("#logout").click(function(){
        
                $.mobile.loading('show', {});

                $.ajax({
                    type: 'GET',
                    url: tracehost+'/user/logout',
                    data: null,
                    success: function(data) {
                                if(data.status){
                                    //alert("login successful");
                                    $.mobile.loading( 'hide', {});

                                    if (deviceSupportsLocalStorage()) {

                                        localStorage.setItem("isLogedIn", 'false');
                                        localStorage.setItem("user.home.cached", 'false');
                                        localStorage.setItem("user.work.cached", 'false');
                                    }

                                    $.mobile.changePage("#statPage",{});
                                }
                            },
                    error: ajaxConnectionError,
                    contentType: "application/json",
                    dataType: 'json'
                });
        });
    });
    
    
    $(document).on("pageinit", "#eventForm", function(){
    
        $("#createEvent").click(function(){
    
            var eventcategory = $("input#eventcategory").val();
            var note = $("textarea#eventnote").val();
            var address = $("textarea#eventaddress").val();
            var visibility = $("select#flip-a").val();
            var nameofplace = $("input#nameofplace").val();
            var nearby = $("input#eventnearby").val();
            var city = $("input#eventcity").val();
    
            var ispublic;

            if (visibility == "public") {
                ispublic = true;
            }else{
                ispublic = false;
            }
    
            if(nameofplace.length == 0 || eventcategory.length == 0 || city.length == 0){
               showPopUp('Please fill Name of place, category and city');
                return false;
            }
    
            $.mobile.loading( 'show', {});



            if (deviceSupportsLocalStorage()) {

                if (localStorage.getItem("reg.events.isCreated") != 'true') {
                    $.mobile.loading( 'hide', {});
                    showPopUp('Oopse! Please verify your entry and try again');
                    return false;
                }

                var longitude = localStorage.getItem("reg.events.longitude");
                var latitude = localStorage.getItem("reg.events.latitude");
                var altitude = localStorage.getItem("reg.events.altitude");

                localStorage.setItem("reg.events.isCreated", 'false');//make false after use

                $.ajax({
                    type: 'POST',
                    url: tracehost+'/event/createEvent',
                    data: JSON.stringify({city: city, nearby: nearby, nameofplace: nameofplace, ispublic: ispublic, category: eventcategory, note: note, address: address, longitude: longitude, latitude: latitude, altitude:altitude}),
                    success: function(data) {

                                if(data.status){

                                    $.mobile.loading( 'hide', {});
                                    showPopUp('Success!');

                                    if (deviceSupportsLocalStorage()) {

                                        localStorage.setItem("user.events.cached", 'false');// tell homepage to update cache with internet materia
                                    }

                                    setTimeout(function(){
                                        $.mobile.changePage("#eventpageinit",{});
                                        //window.location = userprofile;
                                    }, 3000);

                                }else{
                                    $.mobile.loading( 'hide', {});
                                    showPopUp('Oopse! Please verify your entry. Traced returned: '+ data.reason);
                                    //alert("error occured");
                                }

                            },
                    error: ajaxConnectionError,
                    contentType: "application/json",
                    dataType: 'json'
                });


            }else{

                $.ajax({
                    type: 'POST',
                    url: tracehost+'/user/event',
                    data: JSON.stringify({city: city, nearby: nearby, nameofplace: nameofplace, ispublic: ispublic, category: eventcategory, note: note, address: address}),
                    success: function(data) {

                                if(data.status){

                                    $.mobile.loading( 'hide', {});
                                    showPopUp('Success!');
                                    setTimeout(function(){
                                        $.mobile.changePage("#eventpageinit",{});
                                        //window.location = userprofile;
                                    }, 3000);

                                }else{
                                    $.mobile.loading( 'hide', {});
                                    showPopUp('Oopse! Please verify your entry. Traced returned: '+ data.reason);
                                    //alert("error occured");
                                }

                            },
                    error: ajaxConnectionError,
                    contentType: "application/json",
                    dataType: 'json'
                });
            }
        });
    
    });  
    

    $(document).on("pageinit", "#mobileRequest", function(){
    
        $("#makeRequest").click(function(){
        
            //var traceid = $("input#codetotrace").val().trim(); //the the element has been swapped off from the DOM - Use HTML local storage later
            var nickname = $("input#mobilenickname").val().trim();
        
            $.mobile.loading('show', {});
        
            $.ajax({
                    type: 'POST',
                    url: tracehost+'/mobile/makeRequest/',
                    data: JSON.stringify({nickname: nickname}),
                    success: function(data) {

                            $.mobile.loading('hide');

                            if(data.status){
                                showPopUp('Your request is pending authorization');
                                setTimeout(function(){
                                    $.mobile.changePage("#profilePage",{});
                                },3000);
                                
                            }else{
                                showPopUp('Oopse! an error occured. server retured: ' + data.reason);
                            }
                    },
                    error: ajaxConnectionError,
                    contentType: "application/json",
                    dataType: 'json'
            });
        
        });
    
    });
        
    
    
    $(document).on("pageinit", "#mobilePage", function(){

    /*
     * Mobile Handler
     */
        $("#CreateMobile").click(function(){
            navigator.geolocation.getCurrentPosition(onGPSReadMobileSuccess, onGPSTrackError, GPSoptions);
            $.mobile.loading('show', {});
        });
    
    
    });
    
    
    $(document).on("pagebeforecreate", "#homePage", function(){
        //call a function here because this event is only fired ones, add a refresh button to call manual the second time
        updateHomeDOM();
    });

    function updateHomeDOM(){

        //console.log("fired!!! pagebeforecreate Manual or authomatic");

        $.mobile.loading('show');

        if (deviceSupportsLocalStorage()) {
            if (localStorage.getItem("user.home.cached") == 'true') {
                showQRCode('home', localStorage.getItem('user.home.traceid'));
                localStorage.setItem("share.traceid", localStorage.getItem('user.home.traceid'));
                $('#hExtraInfo').html("<address><p><em>Trace Code: </em><b> "+ formatTraceCode(localStorage.getItem('user.home.traceid'))+"</b></p>" + "<p><em>Address:</em> "+localStorage.getItem('user.home.address')+"</p>"+ "<p><em>Nearby:</em> "+localStorage.getItem('user.home.nearby')+"</p>"+ "<p><em>City:</em> "+localStorage.getItem('user.home.city')+"</p></address>");
                $.mobile.loading('hide');
                return;
            }
        }
    
        $.ajax({
                type: 'GET',
                url: tracehost+'/home/getMyTraceCode/',
                data: null,
                success: function(data) {

                        if(data.status){
    
                                $.ajax({
                                    type: 'GET',
                                    url: tracehost+'/home/getLocationDetails/' + data.result,
                                    data: null,
                                    success: function(data) {

                                            $.mobile.loading( 'hide', {});
    
                                            if(data.status){

                                                $('#hExtraInfo').html("<address><p><em>Trace Code: </em><b> "+formatTraceCode(data.result.traceid)+"</b></p>" + "<p><em>Address:</em> "+data.result.address+"</p>"+ "<p><em>Nearby:</em> "+data.result.nearby+"</p>"+ "<p><em>City:</em> "+data.result.city+"</p></address>");

                                                if (deviceSupportsLocalStorage()) {

                                                    localStorage.setItem("share.traceid", data.result.traceid);
                                                    localStorage.setItem("user.home.cached", 'true');//make false after use
                                                    localStorage.setItem("user.home.traceid", data.result.traceid);
                                                    localStorage.setItem("user.home.address", data.result.address);
                                                    localStorage.setItem("user.home.nearby", data.result.nearby);
                                                    localStorage.setItem("user.home.city", data.result.city);
                                                }


                                                var content = data.result.traceid;

                                                // var qrcode = "https://chart.googleapis.com/chart?cht=qr&chs="+QRsize+"&chl="+content+"&choe="+QRencoding+"&chld="+QRcorrection;
                                                // $("img#homeqrcode").attr('src', qrcode);

                                                showQRCode('home', content);

                                                
                                            }else{
                                                showPopUp('Update your info to get a trace code');
                                                $('#hExtraInfo').html("<address><p><em>Trace Code:</em><b> "+formatTraceCode("0000000000000000")+"</b></p>"+"</address>");
                                            }
                                    },
                                    error: ajaxConnectionError,
                                    contentType: "application/json",
                                    dataType: 'json'
                            });
                        }else{
                            $.mobile.loading( 'hide', {});
                            $('#hExtraInfo').html("<address><p><em>Trace Code:</em><b> "+formatTraceCode("0000000000000000")+"</b></p>"+"</address>");
                        }
                },
                error: ajaxConnectionError,
                contentType: "application/json",
                dataType: 'json'
        });
    }
    
    $(document).on("pageinit", "#homePage", function(){
        //this event is called once, no problem because the onCLick event has been bound already

        $("#updateHomeLocation").click(function(){
            navigator.geolocation.getCurrentPosition(onGPSReadHomeSuccess, onGPSTrackError, GPSoptions);
            $.mobile.loading('show', {});
        });

        $("#refreshHomeDOM").click(function(){
            updateHomeDOM();
        });

        $("#updateHomeLocation_h").click(function(){
            navigator.geolocation.getCurrentPosition(onGPSReadHomeSuccess, onGPSTrackError, GPSoptions);
            $.mobile.loading('show', {});
        });

        $("#refreshHomeDOM_h").click(function(){
            updateHomeDOM();
        });
    
    
    });
    
    function onGPSReadHomeSuccess(position){

            var altitude = position.coords.altitude;
            var longitude = position.coords.longitude;
            var latitude = position.coords.latitude;
    
            if (latitude.length == 0 || longitude.length == 0) {
                $.mobile.loading( 'hide', {});
                showPopUp('No coordinate available, Please try again');
                return false;
            }


            if (deviceSupportsLocalStorage()) {
                    localStorage.setItem("reg.home.isCreated", 'true');//make false after use
                    localStorage.setItem("reg.home.longitude", longitude);
                    localStorage.setItem("reg.home.latitude", latitude);
                    localStorage.setItem("reg.home.altitude", altitude);
                    $.mobile.loading('hide');
                    
                    showPopUp('Your location has been read.');
                    setTimeout(function(){
                        $.mobile.changePage("#homeForm",{});
                    },3000);

            }else{
    
                    $.ajax({
                        type: 'POST',
                        url: tracehost + '/home/updateLocation',
                        data: JSON.stringify({/*nickname: nickname, */altitude: altitude, longitude: longitude, latitude: latitude}),
                        success: function(data) {
                                    if(data.status){
                                        //get the returned tracecode and store in localHTML5 storage
                                        $.mobile.loading('hide', {});
                                        showPopUp('Your location has been read!');
                                        setTimeout(function(){
                                            $.mobile.changePage("#homeForm",{});
                                        }, 3000);
                                    }else{
                                        $.mobile.loading( 'hide', {});
                                        showPopUp('Oopse! Unable to save your location. Server said: '+ data.reason);
                                        return false;
                                    }
                                },
                        error: ajaxConnectionError,
                        contentType: "application/json",
                        dataType: 'json'
                    });
            }
    
    }
    
    $(document).on("pageinit", "#eventPage", function(){
        $("#readEventLocation").click(function(){
            navigator.geolocation.getCurrentPosition(onGPSReadEventSuccess, onGPSTrackError, GPSoptions);
            $.mobile.loading('show', {});        
        });
    });
    
    
    function onGPSReadEventSuccess(position){
    
                //call cordova here for gps coordinates
                var longitude = position.coords.longitude;
                var latitude = position.coords.latitude;
                var altitude = position.coords.altitude;
    
                if (latitude.length == 0 || longitude.length == 0) {
                    $.mobile.loading( 'hide', {});
                    showPopUp('No coordinate available, Please try again');
                    return false;
                }


                if (deviceSupportsLocalStorage()) {
                    localStorage.setItem("reg.events.isCreated", 'true');//make false after use
                    localStorage.setItem("reg.events.longitude", longitude);
                    localStorage.setItem("reg.events.latitude", latitude);
                    localStorage.setItem("reg.events.altitude", altitude);
                    $.mobile.loading('hide');
                    
                    showPopUp('Your location has been read.');
                    setTimeout(function(){
                        $.mobile.changePage("#eventForm",{});
                    },3000);

                }else{
    
                    $.ajax({
                        type: 'POST',
                        url: tracehost + '/user/eventgeo',
                        data: JSON.stringify({longitude: longitude, latitude: latitude, altitude: altitude}),
                        success: function(data) {
                                    if(data.status){//
                                        $.mobile.loading( 'hide', {});
                                        showPopUp('Your location has been read.');
                                        setTimeout(function(){
                                            $.mobile.changePage("#eventForm",{});
                                        },2000);
                                        
                                        return true;
                                    }else{
                                        $.mobile.loading( 'hide', {});
                                        showPopUp('Oopse! No GPS coordinate available');
                                        return false;
                                    }
                                },
                        error: ajaxConnectionError,
                        contentType: "application/json",
                        dataType: 'json'
                    });
                }
    }
    
   
})(window);