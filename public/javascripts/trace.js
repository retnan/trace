//var host = "localhost:3000";
var host = '';

/*
//PHONEGAP
var userprofile = "user/profile.html";
var userlogin = "user/login.html";
var usereventpage = "user/event.html";
var usermobilepage = 'user/mobile.html';
var Urlworkmap = 'user/workmap.html';
var Urleventmap = 'user/eventmap.html';
var Urlmobilemap = 'user/mobilemap.html';
var Urlhomemap = 'user/tracemap.html';
*/

//DEVELOPMENT
var userprofile = "/static/profile.html";
var userlogin = "/static/login.html";
var usereventpage = "/static/event.html";
var usermobilepage = '/static/mobile.html';
var Urlworkmap = '/static/workmap.html';
var Urleventmap = '/static/eventmap.html';
var Urlmobilemap = '/static/mobilemap.html';
var Urlmobilemultimap = '/static/mobilemultimap.html';
var Urlhomemap = '/static/tracemap.html';
var userhomeform = '/static/homeform.html';
var UrlmobileMakeRequest = '/static/mobilemakerequest.html';
var mobileGlobalTracid = null;
var userworkplaceform = '/static/workplaceform.html';
var usereventform = '/static/eventform.html';


/*
$(document).bind('pageload', function(event, ui) {
  //Find all of the pages and dialogs in the DOM

    var response = ui.xhr.responseText;
    var data = $(response).filter('[data-role="page"], [data-role="dialog"]');

    for (var i = 1; i <= data.length - 1; i++) {
      var current = data.eq(i);

      if (current.attr('id') && !document.getElementById(current.attr('id'))) {
        current.appendTo('body');
      }
    }
});*/

/* GLobal Variables */
var directionsService = new google.maps.DirectionsService()
var directionsDisplay = null;
var gloTargetLatitude = null;
var gloTargetLongitude = null;
var marker = null;
var map = null;


function setUpMap(locationData, panel){
    //setup the global variable to be used later
    gloTargetLatitude = locationData.gps.latitude;
    gloTargetLongitude = locationData.gps.longitude;

    directionsDisplay = new google.maps.DirectionsRenderer();
    //var destLat = 9.933333300000000000;
    //var destLong = 8.883333300000004000;

    var mapcontainer = 'map_canvas';

    var location = new google.maps.LatLng(gloTargetLatitude, gloTargetLongitude);

    var mapOptions = {
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: location
    };

    map = new google.maps.Map(document.getElementById(mapcontainer), mapOptions);

    marker = new google.maps.Marker({
          position: location,
          map: map,
          title: 'Hello World!'
    });

    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById(panel));
    console.log("Rendering google map");
}


function setUpMultiMap(locationDataArray, panel){
    //setup the global variable to be used later

    if(locationDataArray.length == 0){
        return false;
    }

    directionsDisplay = new google.maps.DirectionsRenderer();

    var mapcontainer = 'map_canvas';

    var location = new google.maps.LatLng("9.081999000000000000", "8.675277000000051000");

    var mapOptions = {
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: location
    };

    map = new google.maps.Map(document.getElementById(mapcontainer), mapOptions);

    for (var i = locationDataArray.length - 1; i >= 0; i--) {

        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locationDataArray[i].location_id_a.gps.latitude, locationDataArray[i].location_id_a.gps.longitude),
          map: map,
          title: locationDataArray[i].nickname
        });
    };

    /*marker = new google.maps.Marker({
          position: location,
          map: map,
          title: 'Hello World!'
    });*/

    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById(panel));
    console.log("Rendering google map");
}

function computeRoute(myLat, myLong, mode) {

        var origin = new google.maps.LatLng(myLat, myLong);
        var target = new google.maps.LatLng(gloTargetLatitude, gloTargetLongitude);

        var request = {
            origin: origin,
            destination: target,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                directionsDisplay.setMap(map);
            }
        });
    }


$(document).on("pageinit", "#traceMap", function(){

    var traceid = $("input#codetotrace").val().trim();

   $.ajax({
            type: 'GET',
            url: host+'/home/getLocationDetails/' + traceid,
            data: null,
            success: function(data) {
                
                    if(data.status){
                        $.mobile.loading( 'hide', {});
                        setUpMap(data.result.location_id,'hinfoDirPanel');
                        setTimeout(function(){
                            google.maps.event.trigger(map,'resize');
                            map.setCenter(marker.getPosition());
                            $('#hExtraInfo').html("<address><p><em>Trace Code:</em><b> "+data.result.traceid+"</b></p>" + "<p><em>Address:</em> "+data.result.address+"</p>"+ "<p><em>Nearby:</em> "+data.result.nearby+"</p>"+ "<p><em>City:</em> "+data.result.city+"</p></address>");
                        },2000);
                    }else{
                        //the delay is neccessary to properly display the popup
                        $.mobile.loading('show', {});
                        setTimeout(function(){
                            $.mobile.loading( 'hide', {});
                            $('#map_canvas').remove();
                            $("#retnaninfoBox").popup();
                            $("#infoboxmesg").html('The Trace Code does not exist or as expired.');
                            $("#retnaninfoBox").popup('open');
                        },2000);
                    }
            },
            contentType: "application/json",
            dataType: 'json'
    });


    $("#htrack").click(function(){

        $("#retnaninfoBox").popup();
        navigator.geolocation.getCurrentPosition(onGPSTrackSuccess, onGPSTrackError);
        $.mobile.loading('show', {});
    });

    $("#hinfohandler").click(function(){
        $("#homeinfoBox").popup();
        $("#homeinfoBox").popup('open');
    });

    $('#tracehome').click(function(){
        $("#retnaninfoBox").popup();
        //////////////////////////////
        $("#infoboxmesg").html('Not supported yet!');
        $("#retnaninfoBox").popup('open');
    });

});

function onGPSTrackError(err){

    console.log(err)
    $.mobile.loading( 'hide', {});
    $("#infoboxmesg").html('No cordinate available, Please try again');
    $("#retnaninfoBox").popup('open');
}

function onGPSTrackSuccess(position) {

            //var longitude = '9.845172000000048000';
            //var latitude = '10.309820000000000000';
            var longitude = position.coords.longitude;
            var latitude = position.coords.latitude;
            var altitude = -1;

            if (latitude.length == 0 || longitude.length == 0) {
                $.mobile.loading( 'hide', {});
                $("#infoboxmesg").html('No cordinate available, Please try again');
                $("#retnaninfoBox").popup('open');
                return false;
            };
            //tell google map about my current location
            computeRoute(latitude, latitude, null);
            $.mobile.loading( 'hide', {});
}

$(document).on("pageinit", "#workMap", function(){

    var traceid = $("input#codetotrace").val().trim();

   $.ajax({
            type: 'GET',
            url: host+'/work/getLocationDetails/' + traceid,
            data: null,
            success: function(data) {
                
                    if(data.status){
                        $.mobile.loading( 'hide', {});
                        setUpMap(data.result.location_id,'winfoDirPanel');
                        setTimeout(function(){
                            google.maps.event.trigger(map,'resize');
                            map.setCenter(marker.getPosition());
                            $('#wExtraInfo').html("<address><p><em>Title:</em><b> "+data.result.workname+"</b></p>" + "<p><em>Address:</em> "+data.result.address+"</p>"+ "<p><em>Tag:</em> "+data.result.worktag+"</p>"+ "<p><em>City:</em> "+data.result.city+"</p></address>");
                        },2000);
                    }else{
                        //the delay is neccessary to properly display the popup
                        $.mobile.loading('show', {});
                        setTimeout(function(){
                            $.mobile.loading( 'hide', {});
                            $('#map_canvas').remove();
                            $("#retnaninfoBox").popup();
                            $("#infoboxmesg").html('The Trace Code does not exist or as expired.');
                            $("#retnaninfoBox").popup('open');
                        },2000);
                    }
            },
            contentType: "application/json",
            dataType: 'json'
    });


    $("#wtrack").click(function(){

        $("#retnaninfoBox").popup();
        navigator.geolocation.getCurrentPosition(onGPSTrackSuccess, onGPSTrackError);
        $.mobile.loading('show', {});
    });

    $("#winfohandler").click(function(){
        $("#workinfoBox").popup();
        $("#workinfoBox").popup('open');
    });

    $('#tracework').click(function(){
        $("#retnaninfoBox").popup();
        //////////////////////////////
        $("#infoboxmesg").html('Not supported yet!');
        $("#retnaninfoBox").popup('open');
    });
});

$(document).on("pageinit", "#eventMap", function(){

    var traceid = $("input#codetotrace").val().trim();

   $.ajax({
            type: 'GET',
            url: host+'/event/getLocationDetails/' + traceid,
            data: null,
            success: function(data) {
                
                    if(data.status){
                        $.mobile.loading( 'hide', {});
                        setUpMap(data.result,'einfoDirPanel');
                        setTimeout(function(){
                            google.maps.event.trigger(map,'resize');
                            map.setCenter(marker.getPosition());
                            $('#eExtraInfo').html("<address><p><em>Trace Code:</em><b> "+data.result.traceid+"</b></p>"+  "<p><em>Place:</em> "+data.result.nameofplace+"</p>"+ "<p><em>Event Type:</em> "+data.result.category+"</p>"+ "<p><em>Address:</em> "+data.result.address+"</p>"+ "<p><em>Nearby:</em> "+data.result.location_id.nearby+"</p>"+ "<p><em>City: "+data.result.city+ "<p><em>Note:</em> "+data.result.note+"</p>"+"</em></p></address>");
                        },2000);
                    }else{
                        //the delay is neccessary to properly display the popup
                        $.mobile.loading('show', {});
                        setTimeout(function(){
                            $.mobile.loading( 'hide', {});
                            $('#map_canvas').remove();
                            $("#retnaninfoBox").popup();
                            $("#infoboxmesg").html('The Trace Code does not exist or as expired.');
                            $("#retnaninfoBox").popup('open');
                        },2000);
                    }
            },
            contentType: "application/json",
            dataType: 'json'
    });


    $("#etrack").click(function(){

        $("#retnaninfoBox").popup();
        navigator.geolocation.getCurrentPosition(onGPSTrackSuccess, onGPSTrackError);
        $.mobile.loading('show', {});
    });

    $("#einfohandler").click(function(){
        $("#eventinfoBox").popup();
        $("#eventinfoBox").popup('open');
    });

    $('#traceevent').click(function(){
        $("#retnaninfoBox").popup();
        //////////////////////////////
        $("#infoboxmesg").html('Not supported yet!');
        $("#retnaninfoBox").popup('open');
    });
});

$(document).on("pageinit", "#mobilepageinit", function(){

    $("#mobileDestroyer").click(function(){

        $.ajax({
                type: 'GET',
                url: host+'/mobile/destroyMe/',
                data: null,
                success: function(data) {
                        if(data.status){
                            $("a#mobileDestroyer").attr('disabled', 'disabled');
                            $("a#mobileBtn").attr('href', usermobilepage);
                            $('#mExtraInfo').html("<address><p><em>Trace Code:</em><b>0000000000000000000000000000000</p></address>");
                            $("#retnaninfoBox").popup();
                            $("#infoboxmesg").html('Mobile details destroyed');
                            $("#retnaninfoBox").popup('open');
                        }else{
                            $("#retnaninfoBox").popup();
                            $("#infoboxmesg").html('Oopse! Server said: ' + data.reason);
                            $("#retnaninfoBox").popup('open');
                        }
                },
                contentType: "application/json",
                dataType: 'json'
        });
    });

    $('#seeTargetonMap').click(function(){

        if (mobileGlobalTracid != 'undefined' || mobileGlobalTracid != null) {
            $.mobile.changePage(Urlmobilemap,{});
        }else{
            $("#retnaninfoBox").popup();
            $("#infoboxmesg").html('Oopse! an error occured, please hit refresh');
            $("#retnaninfoBox").popup('open');
        }
    });

    
    $("#seeTargetsonMap").click(function(){ //note the plural

        $.ajax({
                type: 'GET',
                url: host+'/mobile/canSeeTargetsOnMultiMap/',
                data: null,
                success: function(data) {
                        if(data.status){
                            $.mobile.changePage(Urlmobilemultimap,{});
                        }else{
                            $("#retnaninfoBox").popup();
                            $("#infoboxmesg").html('Access denied! To access targets, create a mobile trace code and share.');
                            $("#retnaninfoBox").popup('open');
                        }
                },
                contentType: "application/json",
                dataType: 'json'
        });
    });

});

function AuthorizeMobileRequest(doc_id){

    $.mobile.loading('show', {});

    $.ajax({
                type: 'GET',
                url: host+'/mobile/authorize/' + doc_id,
                data: null,
                success: function(data) {
                        if(data.status){
                            $.mobile.loading('hide');
                            $('#refreshCheckRequest').show();
                            $('#authorizeIt').hide();
                            $("#retnaninfoBox").popup();
                            $('#mRequestInfo').html("You currently do not have request. Hit refresh to see if there's more");
                            $("#infoboxmesg").html("Authorized! click 'see target' to see target on your map");
                            $("#retnaninfoBox").popup('open');
                        }else{
                            $.mobile.loading('hide');
                            $("#retnaninfoBox").popup();
                            $("#infoboxmesg").html('Sorry, we were unable to authorize user, please try again: Server said: ' + data.reason);
                            $("#retnaninfoBox").popup('open');
                        }
                },
                contentType: "application/json",
                dataType: 'json'
        });
}

$("#mobilepageinit").live("pagebeforecreate", function(){


$('#authorizeIt').hide();

    $.ajax({
            type: 'GET',
            url: host+'/mobile/canCreate/',
            data: null,
            success: function(data) {

                    if(data.status){
                        $("a#mobileBtn").attr('href', '#');
                        $('#mExtraInfo').html("<address><p><em>Trace Code:</em><b> "+data.traceid+"</p></address>");
                        //$('#mtracenumber').html(data.traceid);
                    }else{
                        console.log("Error: "+data.reason);
                        $('#mExtraInfo').html("<address><p><em>Trace Code:</em><b>0000000000000000000000000000000</p></address>");
                    }
            },
            contentType: "application/json",
            dataType: 'json'
    });


    $.ajax({
            type: 'GET',
            url: host+'/mobile/getRequest',
            data: null,
            success: function(data) {

                    if(data.status){
                        //only one record supported for now. using findOne wrapper
                        //for (var i = data.result.length - 1; i >= 0; i--) {
                        //    data.result[i]
                        //};
                        if (data.result.length != 0) {
                            $('#authorizeIt').show();
                            var func = 'AuthorizeMobileRequest("'+data.result[0]._id+'"); return false;';
                            $('#authorizeDynBtn').attr('onClick', func);
                            $('#mRequestInfo').html("You have a request from " + data.result[0].nickname);
                            $('#refreshCheckRequest').hide();
                        };
                        //
                        //
                        
                    }else{
                         //$('#mRequestInfo').html("");
                    }
            },
            contentType: "application/json",
            dataType: 'json'
    });

    $.ajax({
            type: 'GET',
            url: host+'/mobile/getPendingRequest',
            data: null,
            success: function(data) {

                    if(data.status){
                        if(data.result.authorized == '0'){
                            $('#mPendingRequest').html("Sorry! your request to the trace code <b>"+ data.result.traceid_b + "</b> is still pending");
                        }else if(data.result.authorized == '1'){
                            $('#mPendingRequest').html("Congratulations! your request to the trace code <b>"+ data.result.traceid_b + "</b> was accepted");
                            $('#seeTargetonMap').show();
                            mobileGlobalTracid = data.result.traceid_b;
                        }else if(data.result.authorized == '2'){
                            $('#mPendingRequest').html("Sorry! your request to the trace code <b>"+ data.result.traceid_b + "</b> was rejected");
                        }else{
                            $('#mPendingRequest').html("Sorry! your request to the trace code <b>"+ data.result.traceid_b + "</b> has expired");
                        }

                    }
            },
            contentType: "application/json",
            dataType: 'json'
    });

});

$(document).on("pageinit", "#eventpageinit", function(){

    $("#retnaninfoBox").popup();

    $("#eventDestroyer").click(function(){

        $.mobile.loading('show', {});

        $.ajax({
                type: 'GET',
                url: host+'/event/destroyMe/',
                data: null,
                success: function(data) {
                        if(data.status){
                            $.mobile.loading('hide');
                            $("a#eventDestroyer").attr('disabled', 'disabled');
                            $("a#eventBtn").attr('href', usereventpage);
                            $("#infoboxmesg").html('Event details destroyed');
                            $("#retnaninfoBox").popup('open');
                        }else{
                            $.mobile.loading('hide');
                            $("#infoboxmesg").html('Oopse! Server said: ' + data.reason);
                            $("#retnaninfoBox").popup('open');
                        }
                },
                contentType: "application/json",
                dataType: 'json'
        });
    });

});

$("#eventpageinit").live("pagebeforecreate", function(){

    $.mobile.loading('show', {});

    $.ajax({
            type: 'GET',
            url: host+'/event/getActiveEvent/',
            data: null,
            success: function(data) {

                    if(data.status){

                        $("a#eventBtn").attr('href', '#');
                        
                            $.ajax({
                                type: 'GET',
                                url: host+'/event/getLocationDetails/' + data.result.traceid,
                                data: null,
                                success: function(data) {
                                        $.mobile.loading( 'hide', {});
                                        if(data.status){
                                            $('#eExtraInfo').html("<address><p><em>Trace Code:</em><b> "+data.result.traceid+"</b></p>"+  "<p><em>Place:</em> "+data.result.nameofplace+"</p>"+ "<p><em>Event Type:</em> "+data.result.category+"</p>"+ "<p><em>Address:</em> "+data.result.address+"</p>"+ "<p><em>Nearby:</em> "+data.result.location_id.nearby+"</p>"+ "<p><em>City:</em> "+data.result.city+ "<p><em>Note:</em> "+data.result.note+"</p>"+"</p></address>");
                                        }else{
                                            $("#retnaninfoBox").popup();
                                            $("#infoboxmesg").html('The Trace Code does not exist or as expired.');
                                            $("#retnaninfoBox").popup('open');
                                        }
                                },
                                contentType: "application/json",
                                dataType: 'json'
                        });
                    }else{
                        $('#eExtraInfo').html("<address><p><em>Trace Code:</em><b> 0000000000000000000000000000000</b></p>"+"</address>");
                    }
            },
            contentType: "application/json",
            dataType: 'json'
    });
    /*
    $.ajax({
            type: 'GET',
            url: host+'/event/getActiveEvent/',
            data: null,
            success: function(data) {
                    if(data.status){
                        $("a#eventBtn").attr('href', '#');
                        $('#etracenumber').html(data.result.traceid);
                    }else{
                        $('#etracenumber').html('0000000000000000000000000000000');
                    }
            },
            contentType: "application/json",
            dataType: 'json'
    });*/
});



$(document).on("pageinit", "#mobileMap", function(){

    var traceid;

    if (mobileGlobalTracid != null || mobileGlobalTracid != 'undefined') {
        traceid = mobileGlobalTracid;
    }else{
        traceid = $("input#codetotrace").val().trim();
    }

   $.ajax({
            type: 'GET',
            url: host+'/mobile/getBLocationDetails/' + traceid,
            data: null,
            success: function(data) {
                
                    if(data.status){
                        $.mobile.loading( 'hide', {});
                        setUpMap(data.result.location_id_b,'minfoDirPanel');
                        setTimeout(function(){
                            google.maps.event.trigger(map,'resize');
                            map.setCenter(marker.getPosition());
                            $('#mExtraInfo').html("<address><p><em>Trace Code:</em><b> "+data.result.traceid_b+"</b></p>"+ "</address>");
                        },2000);
                    }else{
                        //the delay is neccessary to properly display the popup
                        $.mobile.loading('show', {});
                        setTimeout(function(){
                            $.mobile.loading( 'hide', {});
                            $('#map_canvas').remove();
                            $("#retnaninfoBox").popup();
                            $("#infoboxmesg").html('The Trace Code does not exist or as expired.');
                            $("#retnaninfoBox").popup('open');
                        },2000);
                    }
            },
            contentType: "application/json",
            dataType: 'json'
    });


    $("#mtrack").click(function(){

        $("#retnaninfoBox").popup();
        navigator.geolocation.getCurrentPosition(onGPSTrackSuccess, onGPSTrackError);
        $.mobile.loading('show', {});

    });

    $("#minfohandler").click(function(){
        $("#mobileinfoBox").popup();
        $("#mobileinfoBox").popup('open');
    });

    $('#tracesinglemap').click(function(){
        $("#retnaninfoBox").popup();
        //////////////////////////////
        $("#infoboxmesg").html('Not supported yet!');
        $("#retnaninfoBox").popup('open');
    });
});



$(document).on("pageinit", "#mobilemultiMap", function(){

   $.ajax({
            type: 'GET',
            url: host+'/mobile/getAllAuthorizedLocationDetails/',
            data: null,
            success: function(data) {
                
                    if(data.status){
                        $.mobile.loading( 'hide', {});
                        setUpMultiMap(data.result,'mmultiinfoDirPanel');
                        setTimeout(function(){
                            google.maps.event.trigger(map,'resize');
                            map.setCenter(marker.getPosition());
                            //$('#mmultiExtraInfo').html("<address><p><em>Trace Code:</em><b> "+data.result.traceid_b+"</b></p>"+ "</address>");
                            //perform a loop here
                        },2000);
                    }else{
                        //the delay is neccessary to properly display the popup
                        $.mobile.loading('show', {});
                        setTimeout(function(){
                            $.mobile.loading( 'hide', {});
                            $('#map_canvas').remove();
                            $("#retnaninfoBox").popup();
                            $("#infoboxmesg").html('The Trace Code does not exist or as expired.');
                            $("#retnaninfoBox").popup('open');
                        },2000);
                    }
            },
            contentType: "application/json",
            dataType: 'json'
    });

    $('#tracemultimap').click(function(){
        $("#retnaninfoBox").popup();
        //////////////////////////////
        $("#infoboxmesg").html('Not supported yet!');
        $("#retnaninfoBox").popup('open');
    });


    $("#mmultitrack").click(function(){

        $("#retnaninfoBox").popup();
        //////////////////////////////
        $("#infoboxmesg").html('Not supported yet!');
        $("#retnaninfoBox").popup('open');
        return false;
        ///////////////////

    });

    $("#mmultiinfohandler").click(function(){
        $("#mobileinfoBox").popup();
        $("#mobileinfoBox").popup('open');
    });
});

$(document).on("pageinit", "#tracePage", function(){
/*
 * Trace Map Handler
 */
    $("#traceNow").click(function(){

        var traceid = $("input#codetotrace").val().trim();

        if (traceid.length < 5) {
            $("#retnaninfoBox").popup();
            $("#infoboxmesg").html('Please enter the Trace code');
            $("#retnaninfoBox").popup('open');
            return false;
        };

        $.mobile.loading('show', {})

        $.ajax({
            type: 'GET',
            url: host+'/universe/getLocationType/' + traceid,
            data: null,
            success: function(data) {

                        if(data.status){
                            //1. initialize map here
                            //2. show basic google map here with marker on destination,
                            //3. Do 2. if trace code is home or workplace or event... for mobile location, we must read user's cordinate
                            //because we want the targets to see who ever is coming to them
                            //4. set up buttons on stage and menu for user (Trace, Moving mode, guidience etc)
                            if (data.result == 'home') {
                                //redirect to homemap (Default)
                                $.mobile.changePage(Urlhomemap,{});
                            }else if(data.result == 'work'){
                                //redirect to workmap
                                $.mobile.changePage(Urlworkmap,{});
                            }else if(data.result == 'mobile'){
                                //redirect to mobile - The most sensitive
                                                        
                                    var reqstatus = data.reason;

                                    if(reqstatus == "cantmakerequest"){
                                        $.mobile.loading( 'hide', {});
                                        $("#retnaninfoBox").popup();
                                        $("#infoboxmesg").html('Please, create your mobile trace code first before making a request');
                                        $("#retnaninfoBox").popup('open');
                                    }else if(reqstatus == "norequest"){
                                        $.mobile.changePage(UrlmobileMakeRequest,{});
                                    }else if(reqstatus == "pending"){
                                        $.mobile.loading( 'hide', {});
                                        $("#retnaninfoBox").popup();
                                        $("#infoboxmesg").html('Sorry, your request is still pending authorization');
                                        $("#retnaninfoBox").popup('open');
                                    }else if(reqstatus == "rejected"){
                                        $.mobile.loading( 'hide', {});
                                        $("#retnaninfoBox").popup();
                                        $("#infoboxmesg").html('Sorry, your request has been rejected by target');
                                        $("#retnaninfoBox").popup('open');
                                    }else if(reqstatus == "accepted"){
                                        $.mobile.changePage(Urlmobilemap,{});
                                    }else{
                                        $.mobile.loading( 'hide', {});
                                        $("#retnaninfoBox").popup();
                                        $("#infoboxmesg").html('Sorry, The Trace code has expired or does not exist');
                                        $("#retnaninfoBox").popup('open');
                                    }

                            }else if(data.result == 'events'){
                                console.log("here");
                                $.mobile.changePage(Urleventmap,{});
                                //redirect to eventsmap
                            }else{
                                $.mobile.loading( 'hide', {});
                                $("#retnaninfoBox").popup();
                                $("#infoboxmesg").html('A strange trace code was entered');
                                $("#retnaninfoBox").popup('open');
                            }
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#retnaninfoBox").popup();
                            $("#infoboxmesg").html('The Trace code is invalid or destroyed');
                            $("#retnaninfoBox").popup('open');
                            return false;
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    });
});


$(document).on("pageinit", "#workgeo", function(){

    $("#retnaninfoBox").popup();
    $("#readWorkLocation").click(function(){
        navigator.geolocation.getCurrentPosition(submitWorkCoordinateSuccess, onGPSTrackError);
        $.mobile.loading('show', {});
    });
});


function submitWorkCoordinateSuccess(position){
    //call cordova here for gps cordinates
            var longitude = position.coords.longitude;
            var latitude = position.coords.latitude;
            var altitude = -1;

            if (latitude.length == 0 || longitude.length == 0) {
                $.mobile.loading( 'hide', {});
                $("#infoboxmesg").html('No cordinate available, Please try again');
                $("#retnaninfoBox").popup('open');
                return false;
            }

            $.ajax({
                type: 'POST',
                url: host + '/user/workplacegeo',
                data: JSON.stringify({longitude: longitude, latitude: latitude, altitude: altitude}),
                success: function(data) {
                            if(data.status){//
                                $.mobile.changePage(userworkplaceform,{});
                                return true;
                            }else{
                                //alert("invalid usercode or passcode");
                                $.mobile.loading( 'hide', {});
                                $("#infoboxmesg").html('Oopse! No GPS cordinates available');
                                $("#retnaninfoBox").popup('open');
                                return false;
                            }
                        },
                contentType: "application/json",
                dataType: 'json'
            });
}

$(document).on("pageinit", "#workresult", function(){

    $("#retnaninfoBox").popup();
/*
 * Login Handler
 */
    $("#gpsorcongrat").click(function(){

        $.mobile.loading('show', {});

        var resultid = $('input[name="radiowork"]').val();

        resultid = 0;

        var url;

        if(resultid == 0){
            return true;
        }else{
            //call remote server - End of workplace registration
        }
        return false;
    });
});



$("#work").live("pagebeforecreate", function(){

    $.ajax({
            type: 'GET',
            url: host+'/work/getMyTraceCode/',
            data: null,
            success: function(data) {
                    if(data.status){
                        
                            $.ajax({
                                type: 'GET',
                                url: host+'/work/getLocationDetails/' + data.result,
                                data: null,
                                success: function(data) {

                                    console.log(data)
                                        $.mobile.loading( 'hide', {});

                                        if(data.status){
                                             $('#wExtraInfo').html("<address><p><em>Title:</em><b> "+data.result.workname+"</b></p>" + "<p><em>Trace Code:</em> "+data.result.traceid+"</p>" + "<p><em>Address:</em> "+data.result.address+"</p>"+ "<p><em>Tag:</em> "+data.result.worktag+"</p>"+ "<p><em>City:</em> "+data.result.city+"</p></address>");
                                        }else{
                                            $("#retnaninfoBox").popup();
                                            $("#infoboxmesg").html('Update your info to get a trace code');
                                            $("#retnaninfoBox").popup('open');
                                            $('#wExtraInfo').html("<address><p><em>Trace Code:</em><b> 0000000000000000000000000000000</b></p>"+"</address>");
                                        }
                                },
                                contentType: "application/json",
                                dataType: 'json'
                        });
                    }else{
                        $('#wExtraInfo').html("<address><p><em>Title:</em><b> 0000000000000000000000000000000</b></p>"+"</address>");
                    }
            },
            contentType: "application/json",
            dataType: 'json'
    });

});

$(document).on("pageinit", "#workPage", function(){

$("#retnaninfoBox").popup();

/*
 * Login Handler
 */
    $("#searchworkplace").click(function(){

        var workname = $("input#workname").val();
        var city = $("input#city").val();

        if(workname.length == 0 || city.length == 0){
            $("#infoboxmesg").html('Entry is required');
            $("#retnaninfoBox").popup('open');
            return false;
        }

        $.mobile.loading('show', {});

        $.ajax({
            type: 'POST',
            url: host + '/user/workplacesearch',
            data: JSON.stringify({workname: workname, city: city}),
            success: function(data) {
                        if(data.status){
                            return true
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Error, Please try again later');
                            $("#retnaninfoBox").popup('open');
                            return false;
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    }); 
});

$(document).on("pageinit", "#homeForm", function(){

$("#retnaninfoBox").popup();

/*
 * Home update Handler
 */
    $("#createHome").click(function(){

        var address = $("textarea#address").val();
        var city = $("input#city").val();
        var nearby = $("input#nearby").val();

        if(city.length == 0){
            $("#infoboxmesg").html('City is a required field');
            $("#retnaninfoBox").popup('open');
            return false;
        }

        $.mobile.loading('show', {});

        $.ajax({
            type: 'POST',
            url: host + '/home/updateDetails',
            data: JSON.stringify({city: city, nearby: nearby, address: address}),
            success: function(data) {
                        if(data.status){

                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Your details have been saved');
                            $("#retnaninfoBox").popup('open');

                            setTimeout(function(){
                                $.mobile.changePage(userprofile,{});
                            },3000);

                            return;
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Error, Please try again later');
                            $("#retnaninfoBox").popup('open');
                            return false;
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    }); 
});


$(document).on("pageinit", "#workform", function(){

    $("#retnaninfoBox").popup();

    $("#createWork").click(function(){

        $.mobile.loading('show', {});

        var address = $("textarea#address").val();
        var worktag = $("input#worktag").val();
        var nearby = $("input#nearby").val();


        if(worktag.length == 0){
            $.mobile.loading( 'hide', {});
            $("#infoboxmesg").html('Work Tag is compulsory');
            $("#retnaninfoBox").popup('open');
            return false;
        }


        $.ajax({
            type: 'POST',
            url: host+'/user/workplace',
            data: JSON.stringify({worktag: worktag, nearby: nearby, address: address}),
            success: function(data) {
                        if(data.status){
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Success!');
                            $("#retnaninfoBox").popup('open');
                            setTimeout(function(){
                                $.mobile.changePage(userprofile,{});
                            }, 3000);
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Oopse! Please verify your entry and try again. Traced returned: '+ data.reason);
                            $("#retnaninfoBox").popup('open');
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    });
 });


$(document).on("pageinit", "#loginPage", function(){

$("#retnaninfoBox").popup();
/*
 * Login Handler
 */
    $("#authenticate").click(function(){

        var userCode= $("input#usercode").val();
        var passCode = $("input#passcode").val();

        $.mobile.loading('show', {});

        $.ajax({
            type: 'POST',
            url: host+'/user/login',
            data: JSON.stringify({usercode: userCode, passcode: passCode}),
            success: function(data) {
                        if(data.status){
                            //initialize all that is needed through HMTL5 local or session storage before redirecting
                            $.mobile.changePage(userprofile,{});
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Username or password does not exist');
                            $("#retnaninfoBox").popup('open');
                            return false;
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    }); 
});


$(document).on("pageinit", "#registerPage", function(){

    $("#retnaninfoBox").popup();
/*
 * Registration Handler
 */
    $("#reguser").click(function(){

        //disable the register button

        var userCode= $("input#usercode").val().trim();
        var passCode = $("input#passcode").val().trim();

        if(userCode.length == 0 || passCode.length == 0 ){
            $("#infoboxmesg").html('Please fill in the required details - Everything!');
            $("#retnaninfoBox").popup('open');
            return false;
        }

        $.mobile.loading('show', {});

        $.ajax({
            type: 'POST',
            url: host+'/user/register' + '0',
            data: JSON.stringify({usercode: userCode, passcode: passCode}),
            success: function(data) {
                        if(data.status){
                            $.mobile.loading( 'hide', {});
                            return true;
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Oopse! Username is already taken. Please verify your entry and try again. Traced returned: '+ data.reason);
                            $("#retnaninfoBox").popup('open');
                            return false;
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    });

});



$(document).on("pageinit", "#security0", function(){

    $("#retnaninfoBox").popup();
/*
 * Registration Handler
 */
    $("#register0").click(function(){

        var security0 = $("input#security0").val().trim();

        if(security0.length == 0){
            $("#infoboxmesg").html('Entry is required');
            $("#retnaninfoBox").popup('open');
            return false;
        }

        $.mobile.loading('show', {});

        $.ajax({
            type: 'POST',
            url: host+'/user/register'+'1',
            data: JSON.stringify({animal: security0}),
            success: function(data) {
                        if(data.status){
                            $.mobile.loading( 'hide', {});
                            return true;
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Entry is required');
                            $("#retnaninfoBox").popup('open');
                            return false;
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    });

});


$(document).on("pageinit", "#security1", function(){

    $("#retnaninfoBox").popup();
/*
 * Registration Handler
 */
    $("#register1").click(function(){

        //disable the register button

        var security1 = $("input#security1").val().trim();

        if(security1.length == 0){
            $("#infoboxmesg").html('Entry is required');
            $("#retnaninfoBox").popup('open');
            return false;
        }

        $.mobile.loading('show', {});

        $.ajax({
            type: 'POST',
            url: host+'/user/register'+'2',
            data: JSON.stringify({mother: security1}),
            success: function(data) {
                        if(data.status){
                            console.log("confirmed");
                            $.mobile.loading( 'hide', {});
                            return true;
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Entry is required');
                            $("#retnaninfoBox").popup('open');
                            return false;
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    });

});

$(document).on("pageinit", "#security2", function(){

    $("#retnaninfoBox").popup();
/*
 * Registration Handler
 */
    $("#register2").click(function(){

        //disable the register button

        var security2 = $("input#security2").val().trim();

        if(security2.length == 0){
            $("#infoboxmesg").html('Entry is required');
            $("#retnaninfoBox").popup('open');
            return false;
        }

        $.mobile.loading('show', {});

        $.ajax({
            type: 'POST',
            url: host+'/user/register'+'3',
            data: JSON.stringify({space: security2}),
            success: function(data) {
                        if(data.status){
                            $.mobile.loading( 'hide', {});
                            return true;
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Entry is required');
                            $("#retnaninfoBox").popup('open');
                            return false;
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    });

});

$(document).on("pageinit", "#security3", function(){

    $("#retnaninfoBox").popup();
/*
 * Registration Handler
 */
    $("#register3").click(function(){

        //disable the register button

        var security3 = $("input#security3").val().trim();

        if(security3.length == 0){
            $("#infoboxmesg").html('Entry is required');
            $("#retnaninfoBox").popup('open');
            return false;
        }

        $.mobile.loading('show', {});

        $.ajax({
            type: 'POST',
            url: host+'/user/register'+'4',
            data: JSON.stringify({honeymoon: security3}),
            success: function(data) {
                        if(data.status){
                            $.mobile.loading( 'hide', {});
                            return true;
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Entry is required');
                            $("#retnaninfoBox").popup('open');
                            return false;
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    });

});

$(document).on("pageinit", "#security4", function(){

    $("#retnaninfoBox").popup();
/*
 * Registration Handler
 */
    $("#register4").click(function(){

        //disable the register button

        var security4 = $("input#security4").val().trim();

        if(security4.length == 0){
            $("#infoboxmesg").html('Entry is required');
            $("#retnaninfoBox").popup('open');
            return false;
        }

        $.mobile.loading('show', {});

        $.ajax({
            type: 'POST',
            url: host+'/user/register'+'5',
            data: JSON.stringify({book: security4}),
            success: function(data) {
                        if(data.status){
                        $.ajax({
                            type: 'GET',
                            url: host+'/user/register',
                            data: null,
                            success: function(data) {
                                        if(data.status){
                                            $.mobile.loading( 'hide', {});
                                            $("#infoboxmesg").html('Registration successful');
                                            $("#retnaninfoBox").popup('open');
                                            setTimeout(function(){
                                                $.mobile.changePage(userlogin,{});
                                            }, 3000);
                                        }else{
                                            $.mobile.loading( 'hide', {});
                                            $("#infoboxmesg").html('Entry is required');
                                            $("#retnaninfoBox").popup('open');
                                            return false;
                                        }
                                    },
                            contentType: "application/json",
                            dataType: 'json'
                    });
                        }else{
                            $.mobile.loading( 'hide', {})
                            $("#infoboxmesg").html('Entry is required');
                            $("#retnaninfoBox").popup('open');
                            return false;
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    });

});
/*
$(document).on("pageinit", "#registerPageMain", function(){

    $("#retnaninfoBox").popup();

    $("#register").click(function(){

        //disable the register button

        var userCode= $("input#usercode").val().trim();
        var passCode = $("input#passcode").val().trim();
        var security0 = $("input#security0").val().trim();
        var security1 = $("input#security1").val().trim();
        var security2 = $("input#security2").val().trim();
        var security3 = $("input#security3").val().trim();
        var security4 = $("input#security4").val().trim();



        if(userCode.length == 0 || passCode.length == 0 || security0.length == 0 || security1.length == 0 || security2.length == 0 || security3.length == 0 || security4.length == 0){
            $("#infoboxmesg").html('Please fill in the required details - Everything!');
            $("#retnaninfoBox").popup('open');
            return;
        }



        $.mobile.loading('show', {});

        $.ajax({
            type: 'POST',
            url: host+'/user/register',
            data: JSON.stringify({usercode: userCode, passcode: passCode, animal: security0, mother: security1, space: security2, honeymoon: security3, book: security4}),
            success: function(data) {
                        if(data.status){
                            $("#reghidden").show();
                            $("#secheading").remove();
                            $("#form5").remove();
                            $("#infoboxmesg").html('Success!! Please proceed to login!');
                            $("#retnaninfoBox").popup('open');
                            $.mobile.loading( 'hide', {});
                            setTimeout(function(){
                                $.mobile.changePage(userlogin,{});
                                //window.location = userlogin;
                            }, 3000);
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Oopse! Username is already taken. Please verify your entry and try again. Traced returned: '+ data.reason);
                            $("#retnaninfoBox").popup('open');
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    });

});

*/


$(document).on("pageinit", "#profilePage", function(){

/*
 * SMS Handler
 */
    $("#shareViaSMS").click(function(){
        //call phoneGap here

    });


$(document).on("pageinit", "#settings", function(){

/*
 * Logout Handler
 */
    $("#logout").click(function(){

        $.mobile.loading('show', {})
        $.ajax({
            type: 'GET',
            url: host+'/user/logout',
            data: null,
            success: function(data) {
                        if(data.status){
                            //alert("login successful");
                            $.mobile.loading( 'hide', {})
                            $.mobile.changePage(userlogin,{})
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    });


});
})


$(document).on("pageinit", "#eventForm", function(){

    $("#retnaninfoBox").popup()

    $("#createEvent").click(function(){

        var eventcategory = $("input#category").val()
        var note = $("textarea#note").val()
        var address = $("textarea#address").val()
        var visibility = $("select#flip-a").val()
        var nameofplace = $("input#nameofplace").val()
        var nearby = $("input#nearby").val()
        var city = $("input#city").val()

        var ispublic
        if (visibility == "public") {
            ispublic = true
        }else{
            ispublic = false
        }

        if(nameofplace.length == 0 || eventcategory.length == 0 || city.length == 0){
            $("#infoboxmesg").html('Please fill Name of place, category and city')
            $("#retnaninfoBox").popup('open')
            return false;
        }

        $.mobile.loading( 'show', {});

        $.ajax({
            type: 'POST',
            url: host+'/user/event',
            data: JSON.stringify({city: city, nearby: nearby, nameofplace: nameofplace, ispublic: ispublic, category: eventcategory, note: note, address: address}),
            success: function(data) {
                        if(data.status){
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Success!');
                            $("#retnaninfoBox").popup('open');
                            setTimeout(function(){
                                $.mobile.changePage(userprofile,{});
                                //window.location = userprofile;
                            }, 3000);
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Oopse! Please verify your entry. Traced returned: '+ data.reason);
                            $("#retnaninfoBox").popup('open');
                            //alert("error occured");
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    });

});
/*
$(document).on("pageinit", "#eventPageMain", function(){


    $("#createEvent").click(function(){

        var eventcategory = $("input#category").val();
        var note = $("textarea#note").val();
        var address = $("textarea#address").val();
        var altitude = $("input#altitude").val();
        var longitude = $("input#longitude").val();
        var latitude = $("input#latitude").val();
        var visibility = $("select#flip-a").val();
        var nameofplace = $("input#nameofplace").val();
        var nearby = $("input#nearby").val();
        var city = $("input#city").val();

        var ispublic;
        if (visibility == "public") {
            ispublic = true;
        }else{
            ispublic = false;
        }


        if (longitude.length == 0 || latitude.length == 0) {
            $("#infoboxmesg").html('No cordinate available, Please try again');
            $("#retnaninfoBox").popup('open');
            return;
        }

        if(nameofplace.length == 0 || eventcategory.length == 0 || city.length == 0){
            $("#infoboxmesg").html('Please fill in the required details');
            $("#retnaninfoBox").popup('open');
            return;
        }

        if(altitude.length == 0){
            altitude = null;
        }

        $.mobile.loading( 'show', {});

        $.ajax({
            type: 'POST',
            url: userevent,
            data: JSON.stringify({city: city, nearby: nearby, nameofplace: nameofplace, ispublic: ispublic, category: eventcategory, note: note, address: address, altitude: altitude, longitude: longitude, latitude: latitude}),
            success: function(data) {
                        if(data.status){
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Success!');
                            $("#retnaninfoBox").popup('open');
                            setTimeout(function(){
                                $.mobile.changePage(userprofile,{});
                                //window.location = userprofile;
                            }, 3000);
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Oopse! Please verify your entry. Traced returned: '+ data.reason);
                            $("#retnaninfoBox").popup('open');
                            //alert("error occured");
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    });

});


*/


$(document).on("pageinit", "#mobileRequest", function(){



$("#makeRequest").click(function(){

    //var traceid = $("input#codetotrace").val().trim(); //the the element has been swapped off from the DOM - Use HTML local storage later
    var nickname = $("input#mobilenickname").val().trim();

    $.mobile.loading('show', {});

   $.ajax({
            type: 'POST',
            url: host+'/mobile/makeRequest/',
            data: JSON.stringify({nickname: nickname}),
            success: function(data) {
                
                    if(data.status){
                        $.mobile.loading( 'hide', {});
                        $("#retnaninfoBox").popup();
                        $("#infoboxmesg").html('Your request is pending authorization');
                        $("#retnaninfoBox").popup('open');
                        setTimeout(function(){
                            $.mobile.changePage(userprofile,{});
                        },3000)
                        
                    }else{
                        $.mobile.loading('hide', {});
                        $("#retnaninfoBox").popup();
                        $("#infoboxmesg").html('Oopse! an error occured. server retured: ' + data.reason);
                        $("#retnaninfoBox").popup('open');
                    }
            },
            contentType: "application/json",
            dataType: 'json'
    });

});

});
    


$(document).on("pageinit", "#mobilePage", function(){

/*
 * Mobile Handler
 */
    $("#CreateMobile").click(function(){
         $("#retnaninfoBox").popup();
        navigator.geolocation.getCurrentPosition(onGPSReadMobileSuccess, onGPSTrackError);
        $.mobile.loading('show', {});
    });


});

function onGPSReadMobileSuccess(position){
    //var nickname = -1;
        var altitude = position.coords.altitude;
        var longitude = position.coords.longitude;
        var latitude = position.coords.latitude;

        if (latitude.length == 0 || longitude.length == 0) {
            $.mobile.loading( 'hide', {});
            $("#infoboxmesg").html('No cordinate available, Please try again');
            $("#retnaninfoBox").popup('open');
            return false;
        }

        $.ajax({
            type: 'POST',
            url: host+'/user/mobile',
            data: JSON.stringify({/*nickname: nickname, */altitude: altitude, longitude: longitude, latitude: latitude}),
            success: function(data) {
                        if(data.status){
                            //get the returned tracecode and store in localHTML5 storage
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Success!');
                            $("#retnaninfoBox").popup('open');
                            setTimeout(function(){
                                $.mobile.changePage(userprofile,{});
                            }, 3000);
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Oopse! Please verify your entry and try again. Traced returned: '+ data.reason);
                            $("#retnaninfoBox").popup('open');
                            return false;
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
}


$("#homePage").live("pagebeforecreate", function(){

    $.mobile.loading('show', {});

    $.ajax({
            type: 'GET',
            url: host+'/home/getMyTraceCode/',
            data: null,
            success: function(data) {
                    if(data.status){

                            $.ajax({
                                type: 'GET',
                                url: host+'/home/getLocationDetails/' + data.result,
                                data: null,
                                success: function(data) {
                                        $.mobile.loading( 'hide', {});

                                        if(data.status){
                                            $('#hExtraInfo').html("<address><p><em>Trace Code:</em><b> "+data.result.traceid+"</b></p>" + "<p><em>Address:</em> "+data.result.address+"</p>"+ "<p><em>Nearby:</em> "+data.result.nearby+"</p>"+ "<p><em>City:</em> "+data.result.city+"</p></address>");
                                        }else{
                                            $("#retnaninfoBox").popup();
                                            $("#infoboxmesg").html('Update your info to get a trace code');
                                            $("#retnaninfoBox").popup('open');
                                            $('#hExtraInfo').html("<address><p><em>Trace Code:</em><b> 0000000000000000000000000000000</b></p>"+"</address>");
                                        }
                                },
                                contentType: "application/json",
                                dataType: 'json'
                        });
                    }else{
                        $('#hExtraInfo').html("<address><p><em>Trace Code:</em><b> 0000000000000000000000000000000</b></p>"+"</address>");
                    }
            },
            contentType: "application/json",
            dataType: 'json'
    });

});

$(document).on("pageinit", "#homePage", function(){


    $("#retnaninfoBox").popup();

/*
 * Mobile Handler
 */
    $("#updateHomeLocation").click(function(){

        navigator.geolocation.getCurrentPosition(onGPSReadHomeSuccess, onGPSTrackError);
        $.mobile.loading('show', {});
    });


});

function onGPSReadHomeSuccess(position){

     //var nickname = -1;
     console.log(position)
        var altitude = position.coords.altitude;
        var longitude = position.coords.longitude;
        var latitude = position.coords.latitude;

        if (latitude.length == 0 || longitude.length == 0) {
            $.mobile.loading( 'hide', {});
            $("#infoboxmesg").html('No cordinate available, Please try again');
            $("#retnaninfoBox").popup('open');
            return false;
        }

        $.ajax({
            type: 'POST',
            url: host + '/home/updateLocation',
            data: JSON.stringify({/*nickname: nickname, */altitude: altitude, longitude: longitude, latitude: latitude}),
            success: function(data) {
                        if(data.status){
                            //get the returned tracecode and store in localHTML5 storage
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Your location has been read!');
                            $("#retnaninfoBox").popup('open');
                            setTimeout(function(){
                                $.mobile.changePage(userhomeform,{});
                            }, 3000);
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Oopse! Unable to save your location. Server said: '+ data.reason);
                            $("#retnaninfoBox").popup('open');
                            return false;
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });

}

$(document).on("pageinit", "#eventPage", function(){

    $("#retnaninfoBox").popup();
/*
 * Login Handler
 */
    $("#readEventLocation").click(function(){

        navigator.geolocation.getCurrentPosition(onGPSReadEventSuccess, onGPSTrackError);
        $.mobile.loading('show', {});        
    });
});


function onGPSReadEventSuccess(position){

            //call cordova here for gps cordinates
            var longitude = position.coords.longitude;
            var latitude = position.coords.latitude;
            var altitude = position.coords.altitude;

            if (latitude.length == 0 || longitude.length == 0) {
                $.mobile.loading( 'hide', {});
                $("#infoboxmesg").html('No cordinate available, Please try again');
                $("#retnaninfoBox").popup('open');
                return false;
            }

            $.ajax({
                type: 'POST',
                url: host + '/user/eventgeo',
                data: JSON.stringify({longitude: longitude, latitude: latitude, altitude: altitude}),
                success: function(data) {
                            if(data.status){//
                                $.mobile.changePage(usereventform,{});
                                return true;
                            }else{
                                //alert("invalid usercode or passcode");
                                $.mobile.loading( 'hide', {});
                                $("#infoboxmesg").html('Oopse! No GPS coordinate available');
                                $("#retnaninfoBox").popup('open');
                                return false;
                            }
                        },
                contentType: "application/json",
                dataType: 'json'
            });
}

/*
$(document).on("pageinit", "#workPageMain", function(){

$("#retnaninfoBox").popup();

 console.log("work handler created");

    $("#createWork").click(function(){

        console.log("create work clicked");

        var workname = $("input#workname").val();
        var city = $("input#city").val();
        var address = $("textarea#address").val();
        var altitude = $("input#altitude").val();
        var longitude = $("input#longitude").val();
        var latitude = $("input#latitude").val();
        var worktag = $("input#worktag").val();
        var nearby = $("input#nearby").val();

        if (longitude.length == 0 || latitude.length == 0) {
            $("#infoboxmesg").html('No cordinate available, Please try again');
            $("#retnaninfoBox").popup('open');
            return;
        }

        if(workname.length == 0 || city.length == 0){
            $("#infoboxmesg").html('Please fill in the required details');
            $("#retnaninfoBox").popup('open');
            return;
        }

        if(altitude.length == 0){
            altitude = null;
        }

        $.mobile.loading('show', {});

        $.ajax({
            type: 'POST',
            url: userwork,
            data: JSON.stringify({existing: false, worktag: worktag, nearby: nearby, workname: workname, city: city, address: address, altitude: altitude, longitude: longitude, latitude: latitude}),
            success: function(data) {
                        if(data.status){
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Success!');
                            $("#retnaninfoBox").popup('open');
                            setTimeout(function(){
                                $.mobile.changePage(userprofile,{});
                                //window.location = userprofile;
                            }, 3000);
                        }else{
                            $.mobile.loading( 'hide', {});
                            $("#infoboxmesg").html('Oopse! Please verify your entry and try again. Traced returned: '+ data.reason);
                            $("#retnaninfoBox").popup('open');
                        }
                    },
            contentType: "application/json",
            dataType: 'json'
        });
    });


});

*/