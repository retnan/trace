
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var RedisStore = require('connect-redis')(express);

var app = express();

/*
 * Initializing redis session
 */

app.use(express.cookieParser());

if (/*'development' == app.get('env')*/false) {

	app.use(express.session({
        store: new RedisStore({
        host: process.env.IP,
        port: 6379,//localhost
    }),
    secret: 'wayo'
	}));

}else{

	//redislab.com
	app.use(express.session({
        store: new RedisStore({
        host: 'pub-redis-11955.eu-west-1-1.2.ec2.garantiadata.com',
        port: '11955',
        db: 'traceApp',
        pass: 'Y@hw3hpr0vid35'
    }),
    secret: 'albatakabragsaretartasrfhjoptds'
	}));


}

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());

	var fs = require('fs');
	var jsDir = process.cwd() + '/public/javascripts/';

	var jsfile =  jsDir + "tracesingle.js";
	var minifiedjsfile = jsDir + "tracesingle.min.js";

	var terminal = require('child_process');

	fs.watchFile(jsfile, function(){
		var command = 'java -jar yuicompressor-2.4.6.jar ' + jsfile + ' -o ' + minifiedjsfile
		console.log("++++++++++++++File changed++++++++++++");
		terminal.exec(command, function(err, stout, serr){
			if (!err) {
				console.log("Succesfully generated " + minifiedjsfile);
			}else{
				console.log("An error occured while creating mini-js-file Exit Code: " + err.code + ", Error Dump: " + err);
			}
		})

	});

}





/*
 * Initializing MongoDB Models and Routines
 */

var mongoose = require('mongoose');
var validate = require('mongoose-validator').validate;

if(/*'development' == app.get('env')*/false) {
	mongoose.connect("mongodb://localhost/trace")
}else{
	//mongoose.connect("mongodb://traceapp:Y@hw3hpr0vid35@ds041248.mongolab.com:41248/traceapp");
	mongoose.connect("mongodb://traceapptest:yahweh@ds041238.mongolab.com:41238/traceapptest")
}

var Schema = mongoose.Schema

var userSchema = new Schema({
	usercode:   {
					type: [String], 
					index: true,
					unique: true,
					required: true,
					lowercase: true,
					trim: true,
                    validate:   [
									validate({message: "Usercode should be greater than 3 letters"}, 'len', 3, 50)
								]
				},

	passcode:   {
					type: [String],
					required: true,
					lowercase: true,
					trim: true,
                    validate:   [
									validate({message: "Passcode is empty"}, 'notEmpty')
								]
				},

	homeLocation_id: String,
	workplaceLocation_id: String,
	MobileLocation_id: String, /* ObjectIds */
	EventLocation_id: String,

	securityAnimal : String,
	securityMother : String,
	securitySpace : String,
	securityHoneymoon : String,
	securityBook : String,

	timestamp : Date,
	lastupdated: Date,
	enabled: Boolean
});

/*
As a way of crowdsourcing, if users in the future wants to change their workplace location, we create a copy of their old
work details, and mark the traceid as zombie. STILL NEEDS TOROUGH THINKING
There might be no need for that, since other users will fork it
What if the bank seez to exist there
Whatever the case, we might just move the record to backup, until we know exactly what to do with it in the future which means additional replical model must be created for that purpose
*/

var locationSchema = new Schema({
	uid: String,
	traceid: {
					type: [String], 
					index: true,
					unique: true
			},
	gps:    {
				longitude : String,
				latitude : String
			},
	altitude: String,
	altitudeAcuracy: String,
	speed: String,
	timestamp: Date,
	lastupdated: Date,
	expires: Date, //timestamp + 2hrs
	enabled: Boolean,
	locationtype: String, //home, work, mobile, events
	nearby: String, //identified by traceid
	verifyChange: String//to be used by external APIs to check if location had changed.
});



var mobileSchema = new Schema({
	traceid_a: String,//The Target (Usually Stationary)
	traceid_b: String, // The User B is to find User A
	location_id_a: {type: Schema.ObjectId, ref: 'location'},
	location_id_b: {type: Schema.ObjectId, ref: 'location'},
	authorized: String,
	nickname: String,
	timestamp: Date,
	//conversation: String,
	lastupdated: Date,
	enabled: Boolean
});

var eventSchema = new Schema({
	location_id: {type: Schema.ObjectId, ref: 'location'},
	traceid: {
					type: [String], 
					index: true,
					unique: true
			},
	uid: String,
	nameofplace: String, //e.g Guyana Rock Garden
	category: String, // wedding, concert, shows, conference, workshop etc
	note: String,
	address: String,
	timestamp: Date,
	lastupdated: Date,
	expires: String,//e.g epoch times
	ispublic: Boolean,
	city: String
});


var workSchema = new Schema({
	location_id: {type: Schema.ObjectId, ref: 'location' },
	traceid: {
					type: [String], 
					index: true,
					unique: true
			},
	workname: String,
	city: String,
	uid: String,
	worktag: String, // wedding, concert, shows, conference, workshop etc
	nearby: String,
	address: String,
	timestamp: Date,
	lastupdated: Date
});

var homeSchema = new Schema({
	location_id: {type: Schema.ObjectId, ref: 'location' },
	traceid: {
					type: [String], 
					index: true,
					unique: true
			},
	city: String,
	uid: String,
	nearby: String,
	address: String,
	timestamp: Date,
	lastupdated: Date
});

userSchema.set('autoIndex', true);
workSchema.set('autoIndex', true);
homeSchema.set('autoIndex', true);
eventSchema.set('autoIndex', true);
locationSchema.set('autoIndex', true);
					

var UserModel = mongoose.model('users', userSchema);
var LocationModel = mongoose.model('location', locationSchema);
var eventModel = mongoose.model('events', eventSchema);
var workModel = mongoose.model('work', workSchema);
var mobileModel = mongoose.model('mobile', mobileSchema);
var HomeModel = mongoose.model('home', homeSchema);


require('./routes/index')(app);
require('./routes/static')(app);
require('./routes/user')(app, UserModel, LocationModel, eventModel, workModel, HomeModel);
require('./routes/mobile')(app, UserModel, LocationModel, mobileModel);//API
require('./routes/event')(app, UserModel, LocationModel, eventModel);//API
require('./routes/universe')(app, UserModel, LocationModel);//General (universe) API
require('./routes/home')(app, UserModel, LocationModel, HomeModel);//Home API
require('./routes/workplace')(app, UserModel, LocationModel, workModel);//Work API

if (/*'development' == app.get('env')*/false) {

	http.createServer(app).listen(3000, function(){
		console.log('Trace server listening on 3000 Environment: ' + app.get('env'));
	});

}else{

	http.createServer(app).listen(process.env.PORT, function(){
		console.log('Trace server listening on port ' + process.env.PORT);
	});
}
